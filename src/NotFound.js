import React from 'react'
import {Alert,Jumbotron,Container,Row,Col } from "reactstrap"
import propTypes from "prop-types"

 export const NotFound =(props)=> {
 return(
    <div className="container">
        <Jumbotron>
            <Row>
                <Col sm="12" md={{size:6,offset:3}} >
                  <h1> Oups!! Page introuvable !!Vous avez tenté d'accéder à  une page qui ne se trouve pas sur ce site.</h1> 
                  <p>Veuillez revenir à l' <a href="/">ACCUEIL</a></p>
                </Col>
        </Row>
    </Jumbotron>
    </div>
            
          
     

     )
    }

Alert.propTypes={
    color:propTypes.string
}