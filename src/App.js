import React, {Component, Suspense, lazy } from "react";

import { Provider } from "react-redux";
import { Layout } from "./app/@core/utility/context/Layout";

import { store } from "./app/@core/redux/storeConfig/store";
import Spinner from "./app/@vuexy/spinner/Fallback-spinner";
import "./index.scss";



import "./app/@core/@fake-db";
import {Route, Router, Switch} from "react-router";
import {history} from "./history";
import {loadUser} from "./app/@core/redux/actions/auth/loginActions"

import{Provider as AlertProvider} from "react-alert"

import AlertTemplate from 'react-alert-template-basic';
import { NotFound } from "./NotFound";



const alertOptions={
  timeout:5000,
  position:"bottom center"
}



const LazyApp = lazy(() => import("./app/App"));
const LandingApp = lazy(() => import("./landing/App"));


export class App extends Component {

  componentDidMount(){
    store.dispatch(loadUser());

  }

  render() {
    return (
    <Provider store={store}>
        <AlertProvider template={AlertTemplate}{...alertOptions}>
        <Suspense fallback={<Spinner />}>
        <Layout>
          <Router history={history}>
           <Switch>
             <Route path={'/dashboard'}>
            
              <LazyApp />
             </Route>
             <Route exact path={'/'}>
               <LandingApp />
             </Route>
             <Route path="*" component={NotFound}/>
           </Switch>
         </Router>
        </Layout>
      </Suspense>
      </AlertProvider>
    </Provider>
    )
  }
}

export default App