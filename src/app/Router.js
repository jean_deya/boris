import React, {Suspense, lazy} from "react";
import {Router, Switch, Route, Redirect} from "react-router-dom";
import {history} from "../history";
import {connect} from "react-redux";
import Spinner from "./@vuexy/spinner/Loading-spinner";
import {ContextLayout} from "./@core/utility/context/Layout";


const Auth = lazy(() =>
  import("./views/auth/Auth")
);
const ChoixProfile=lazy(()=>
  import("./views/profile/ChoixProfile")
);
const AppNew = lazy(() =>
  import("./views/application/new/New")
);

const AppList = lazy(() =>
  import("./views/application/list/List")
);

const ApplicationDocuments = lazy(() =>
  import("./views/document/Document")
);

const ApplicationDocumentZoom = lazy(() =>
  import("./@vuexy/todos/ZoomDoc")
);

const ApplicationDashboard = lazy(() =>
  import("./views/dashboard/Dashboard")
);



// Set Layout and Component Using App Route

const RouteConfig = (
  {
    component: Component,
    fullLayout,
    landingLayout,
    withoutMenuLayout,
    permission,
    isNotSecure,
    user,
    isAuthenticated,
    token,
    ...rest
  }) => {

  return (
    <Route
      {...rest}
      render={props => {
        console.log(isAuthenticated);
        return (
           isNotSecure ||(token || isAuthenticated )?
            <ContextLayout.Consumer>
              {context => {
                let LayoutTag =
                  fullLayout === true
                    ? context.fullLayout
                    : context.state.activeLayout === "horizontal"
                    ? context.horizontalLayout
                    : context.VerticalLayout;
                LayoutTag = withoutMenuLayout ? context.withoutMenuLayout : LayoutTag;
                return (
                  <LayoutTag {...props} >
                    <Suspense fallback={<Spinner/>}>
                      <Component {...props} />
                    </Suspense>
                  </LayoutTag>
                )
              }}
            </ContextLayout.Consumer>
            : <Redirect to={{pathname: '/dashboard/auth',}}/>
        )
      }}
    />
  );
};

const mapStateToProps = state => {
  return {
    user: state,
    isAuthenticated: state.auth.login.isAuthenticated,
    token: state.auth.login.auth_token
  }
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {
  
 
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Switch>
          <AppRoute
            path="dashboard/ChoixProfile/dashboard/apps/:appId/dashboard"
            component={ApplicationDashboard}
            
          />

          <AppRoute
            path="/dashboard/apps/:appId/documents"
            component={ApplicationDocuments}
          />
           <AppRoute
            exact
            path="/dashboard/apps/:appId/ZoomDoc/:todo_id"
            component={ApplicationDocumentZoom}
          />
          <AppRoute
            exact
            path="/dashboard/apps/new"
            component={AppNew}
            withoutMenuLayout
          />
          <AppRoute
            path="/dashboard/apps/:appId"
            component={ApplicationDashboard}
          />
          
        <AppRoute
            exact
            path="/dashboard/apps"
            component={AppList}
            withoutMenuLayout
          />

          <AppRoute
            
            path="/dashboard/ChoixProfile/:prof_id"
            component={AppList}
            withoutMenuLayout
          />
          
          <AppRoute
            exact
            path="/dashboard/ChoixProfile"
            component={ChoixProfile}
            fullLayout
           
          />
          

          <AppRoute
            exact
            path="/dashboard/auth"
            component={Auth}
            fullLayout
            isNotSecure
          />
          <AppRoute
            component={AppList}
            withoutMenuLayout
          />
           <AppRoute
            component={ChoixProfile}
            fullLayout
          />

          <AppRoute
            component={ApplicationDocumentZoom }
          
          />
        </Switch>
      </Router>
    )
  }
}

export default AppRouter