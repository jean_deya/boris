import React from "react"
import {connect} from "react-redux"
import {Row, Col} from "reactstrap"
import Breadcrumbs from "../../../@vuexy/breadCrumbs/BreadCrumb"
import ListViewConfig from "../../../@vuexy/dataList/DataListConfig"
import queryString from "query-string"
import {getProfile} from "../../../@core/redux/actions/profileActions/profileActions"


class Application extends React.Component {

  componentDidMount(){
   this.props.getProfile(this.props.ProfilId)
   //console.log("ici cest paris", this.props.getProfile(this.props.ProfilId))
  }
  render() {
    console.log("lispar ",this.props.ProfilId)
    return (
      <React.Fragment>
        <Breadcrumbs
          breadCrumbTitle="List View"
          breadCrumbParent="Data List"
          breadCrumbActive="List View"
        />
        <Row>
          <Col sm="12">
            <ListViewConfig parsedFilter={queryString.parse(this.props.location.search)}/>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}
const mapStateToProps=(state,ownProps)=>{  //  l
  let ProfilId= ownProps.match.params.prof_id;    //faire correspondre l id de notre article de blogue a l article du reducer
  return{
 
      ProfilId :ProfilId //retourner un seul post

  };
 
}
export default connect(mapStateToProps,{getProfile})( Application);

