import React from "react";
import {history} from "../../../../history"
import {connect} from "react-redux"
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import {Mail, Package, Server, Shield} from "react-feather";
import {getEntity } from "../../../@core/redux/actions/auth/registerActions"
import {createApp } from "../../../@core/redux/actions/data-list"
import Alert from  "../../../views/auth/Alerts"
class New extends React.Component {
  state={
    newApp:{
      app_name:"",
      app_code:"",
      entity:"",
      created_at:new Date().toLocaleDateString("en-CA"),
      updated_at:"",
      created_by:"",
      updated_by:"",
    }
  }
   
      changeHandler=e=>{
    
        this.setState({
          newApp:{
            ...this.state.newApp,
          [e.target.name]:e.target.value,
          entity:this.props.entityId,
          created_at:this.state.newApp.created_at,
          created_by:this.props.entite,
          updated_at:this.state.newApp.created_at,
          updated_by:this.props.entite,
        }
   })
 }

 submitApp=e=>{
e.preventDefault()
 console.log(this.state.newApp)
   this.props.createApp(this.state.newApp)
 }
componentDidMount(){
  this.props.getEntity(this.props.entityId)
}

  render() {
    console.log("kkddd",this.props.entityId)
    console.log("kk",this.props.entite)
    return (
      <div  className={'mt-5'}>
        
        <Row className={'mr-1 ml-1'}>
        
          <Col lg={5} className={'mt-2'}>
            <Row className="align-items-center">
              <Col lg={{ span: 3, offset: 4 }} md={6}>
                <div className="title-heading mt-4">
                  <h1 className="heading mb-2">Application</h1>
                  <p style={{fontWeight:"300px",fontSize:"18px"}} className="para-desc ">Veuillez ajouter une nouvelle application </p>
                  <p style={{fontWeight:"300px",fontSize:"15px",color:"#ffb21f"}} className="para-desc ">Vous pourriez travailler par la suite sur cette 
                  application avec <br/>toutes les personnes de votre equipe
                  .</p>
                </div>
              </Col>
            </Row>
          </Col>
          <Col lg={7}>
            <div>
              <Card>
                <CardHeader>
                  <CardTitle>Application</CardTitle>
                </CardHeader>
                <CardBody>
                  <form onSubmit={this.submitApp}>
                    <Row>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">Nom de l'application</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="app_name"
                            id="nam"
                            placeholder="Nom de l'application"
                            onChange={this.changeHandler}
                          />
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">code de l'application</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="app_code"
                            id="nameVerticalIcons"
                            placeholder="code de l'application"
                            onChange={this.changeHandler}
                          />
                          <div className="form-control-position">
                            <Server size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      
                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                            //onClick={e => history.push("/dashboard/ChoixProfil")}
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps=(state,ownProps)=>{  //  l
 
  return{
 //key of entity from profilclick
      entityId:state.auth.login.user.id,//retourner un seul post
     entite:state.auth.EntitiesReducer.entity.company_name
  };
 
}
export default connect(mapStateToProps,{getEntity,createApp})( New);
