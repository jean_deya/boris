import React from "react"
import Sidebar from "react-sidebar"
import {ContextLayout} from "../../@core/utility/context/Layout"
import TodoSidebar from "../../@vuexy/todos/TodoSidebar"
import TodoList from "../../@vuexy/todos/TodoList"
import TaskSidebar from "../../@vuexy/todos/TaskSidebar"
import "../../../assets/scss/pages/app-todo.scss"
import axios from"axios"
import {connect} from "react-redux"
import {getProfile} from "../../@core/redux/actions/profileActions/profileActions"


const mql = window.matchMedia(`(min-width: 992px)`);

class Document extends React.Component {


    
    

  state = {
    file:"",
    id:"",
    addTask: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    taskToUpdate: null,
    prevState: null,
    
 }

 componentDidMount(){
  this.props.getProfile()
  axios 
  .get("http://127.0.0.1:8080/v1/files/")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  .then(res=>{
    console.log("info dashboard",res.data.results)
    this.setState({
     file:res.data.results
 
    });
  })
  }

 


  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  };

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  };

  onSetSidebarOpen = open => {
    this.setState({sidebarOpen: open})
  };

  mediaQueryChanged = () => {
    this.setState({sidebarDocked: mql.matches, sidebarOpen: false})
  };

  handleAddTask = status => {
    status === "open"
      ? this.setState({addTask: true})
      : this.setState({addTask: false, taskToUpdate: null})
  };

  handleUpdateTask = todo => {
    if (todo !== undefined) {
      this.setState({addTask: true, taskToUpdate: todo})
    } else {
      this.setState({taskToUpdate: null})
    }
  };

  handleUndoChanges = arr => {
    this.setState({
      prevState: arr
    })
  };
//prendre l'id du document au niveau du todo sidebar
ajout=todoId=>{
  console.log(todoId)
this.setState({
  id:todoId,
  
})
}

  render() {
console.log("document",this.props.profile)
    return (
      <div className="todo-application position-relative">
        <div
          className={`app-content-overlay ${
            this.state.addTask || this.state.sidebarOpen ? "show" : ""
          }`}
          onClick={() => {
            this.handleAddTask("close")
            this.onSetSidebarOpen(false)
          }}
        />
        <ContextLayout.Consumer>
          {context => (
            <Sidebar
              sidebar={
                <TodoSidebar
                file={this.state.file}
                  routerProps={this.props}
                  addTask={this.handleAddTask}
                  mainSidebar={this.onSetSidebarOpen}
                //prendre id
                  data={
                    {id:this.state.id,
                      ajout:this.ajout.bind(this)
                    }
                  }

                />
              }
              docked={this.state.sidebarDocked}
              open={this.state.sidebarOpen}
              sidebarClassName="sidebar-content todo-sidebar d-flex"
              touch={false}
              contentClassName="sidebar-children d-none"
              pullRight={context.state.direction === "rtl"}>
              ""
            </Sidebar>
          )}
        </ContextLayout.Consumer>
        <TodoList
          routerProps={this.props}
          handleUpdateTask={this.handleUpdateTask}
          mainSidebar={this.onSetSidebarOpen}
          prevState={this.state.prevState}
          data={{id:this.state.id}} //recuperer id 
        />
        <TaskSidebar
          addTask={this.handleAddTask}
          addTaskState={this.state.addTask}
          taskToUpdate={this.state.taskToUpdate}
          newTask={this.state.newTask}
          mainSidebar={this.onSetSidebarOpen}
          handleUndoChanges={this.handleUndoChanges}
        />
      </div>
    )
  };
}

const mapStateToProps = state => {
  return {
    dataList: state.dataList,
    //qjouter
    profile:state.profileReducer.profile,
    username:state.auth.login.user.id,

  }
};

export default connect(mapStateToProps,{getProfile})( Document)
