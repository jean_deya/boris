
import React, {Suspense, lazy} from "react";
import { Route, Redirect} from "react-router-dom";

import {connect} from "react-redux";
import Spinner from "../../@vuexy/spinner/Loading-spinner";
import {ContextLayout} from "../../@core/utility/context/Layout";


const RouteConfig = (
    {
      component: Component,
      fullLayout,
      landingLayout,
      withoutMenuLayout,
      permission,
      isNotSecure,
      user,
      isAuthenticated,
      appId,
      ...rest
    }) => {
  
    return (
      <Route
        {...rest}
        render={props => {
          console.log(isAuthenticated);
          return (
            isNotSecure || isAuthenticated ?
              <ContextLayout.Consumer>
                {context => {
                  let LayoutTag =
                    fullLayout === true
                      ? context.fullLayout
                      : context.state.activeLayout === "horizontal"
                      ? context.horizontalLayout
                      : context.VerticalLayout;
                  LayoutTag = withoutMenuLayout ? context.withoutMenuLayout : LayoutTag;
                  return (
                    <LayoutTag {...props} permission={props.user}>
                      <Suspense fallback={<Spinner/>}>
                        <Component {...props} />
                      </Suspense>
                    </LayoutTag>
                  )
                }}
              </ContextLayout.Consumer>
              : <Redirect to={{pathname: '/',}}/>
          )
        }}
      />
    );
  };
  
  const mapStateToProps = state => {
    return {
      user: state.auth,
      isAuthenticated: state.auth.login.isAuthenticated,
      appId:state.dataList.allData.id
    }
  };
  
  export default  connect(mapStateToProps)(RouteConfig);

