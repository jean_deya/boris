import React from "react"
import {addUsers,addEntity,addProfile,listeUsers,getAllEntities} from "../../../@core/redux/actions/auth/registerActions"
import {connect} from "react-redux"
import PropTypes, { object } from "prop-types"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Button,
  Label
} from "reactstrap"
import Alerts from "../Alerts"
import {User, Mail,Lock, Coffee } from "react-feather"
import Axios from "axios"
import { auth } from "firebase"




class Register extends React.Component {
  
  static propTypes={
    error:PropTypes.object.isRequired
  }

  state={
    newUser:{
    first_name:"",
    last_name:"",
    username:"",
    email:"",
    password:"",
    created_at: new Date().toLocaleDateString("en-CA"),
    updated_at:"",
    created_by:"",
    updated_by:"",
    company_name:"",
  },
  Entity:{
    id:"ffff",
    first_name:"",
    last_name:"",
    company_name:"",
    created_at:new Date().toLocaleDateString("en-CA"),
    updated_at:"",
    created_by:"",
    updated_by:"", 

  },
  profil:{
    id:"dffsrf",
    key:"z",
    secret:"z",
    entity:"h",
    users:"25",
    is_first_user:false
    },
    submitted:false
}

  changeHandler=e=>{
    
    this.setState({
      newUser: {
        ...this.state.newUser,
      [e.target.id]:e.target.value,
      created_at:this.state.newUser.created_at,
      created_by:this.state.newUser.username,
      updated_at:this.state.newUser.created_at,
      updated_by:this.state.newUser.username,
      

      },
     
    })
  }

 handleSubmit=e=>{
    e.preventDefault()
    console.log()
    console.log("company",this.state.Entity.company_name)
    //poster sur l'API via le props
    this.setState({submitted:true});
    const{newUser
      }= this.state
        if(newUser.company_name==null ){
          this.setState({
          Entity:{
          ...newUser,
          company_name:newUser.last_name + newUser.username}
        })
    this.props.addUsers(newUser)
    

      
      }else{
        
    this.props.addUsers(newUser)
  
  
      }}
  componentWillMount(){
 
    this.setState({
      first_name:"",
      last_name:"",
      username:"",
      company_name:"",
      email:"",
      password:"",
      created_at:"",
      updated_at:"",
      created_by:"",
      updated_by:""
    })
  }
componentDidMount(){
  this.props.listeUsers()
  //console.log(this.props.listeUsers())
  this.props.getAllEntities()
  console.log("all entites view",this.props.getAllEntities())
}

  render() {
    console.log("liste enti",this.props.entities)
    return (
      
      <Card>
        <CardHeader>
      <Alerts/>
          <CardTitle>Inscription</CardTitle>
        </CardHeader>
        <CardBody>
          
        <form onSubmit={this.handleSubmit}>
            <Row>
         
              <Col sm="12">
                <Label for="nameVerticalIcons">Nom</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="first_name"
                    placeholder="Nom"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="nameVerticalIcons">Prenom</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="last_name"
                    placeholder="Prenom"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="nameVerticalIcons">Pseudo</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="username"
                    placeholder="Pseudo"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="nameVerticalIcons">Nom Entreprise</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="company_name"
                    placeholder="facultative"
                    onChange={e=>{
                      this.setState({
                        newUser: {
                          ...this.state.newUser,
                          company_name: e.target.value
                        }
                    })

                    }}
                  />
                  <div className="form-control-position">
                    <Coffee size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
             
                <Label for="EmailVerticalIcons">Email</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="email"
                    name="Email"
                    id="email"
                    placeholder="Email"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <Mail size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="password"
                    placeholder="Mot de passe"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                </FormGroup>
              </Col>
            
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  //  onClick={e => e.preventDefault()}
                  >
                    S'inscrire
                  </Button.Ripple>
                </FormGroup>
              </Col>
             
            </Row>
          </form>
        </CardBody>
      </Card>

    )
  }
}

Register.propTypes={
  addUsers:PropTypes.func.isRequired,
}




const mapStatetoProps=state=>{
  return{
   // iduser:state.auth.login.id
  id:state.auth.register.id,
  entities:state.auth.EntitiesReducer.entities
  }
}

export default connect(mapStatetoProps,{addUsers,getAllEntities,addEntity,addProfile,listeUsers})( Register)


