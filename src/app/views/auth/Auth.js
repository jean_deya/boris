import React from "react";
import Alerts from "./Alerts"
import {Nav, NavItem, NavLink, TabContent, TabPane, Row, Col} from "reactstrap";
import classnames from "classnames";
import Register from "./register/Register";
import Login from "./login/Login";



class Auth extends React.Component {
  state = {
    active: "1"
  };

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  };

  render() {
    return (

        <div className={'mt-5'}>
         
               
                  
              
          <Row className={'mr-1 ml-1'}>
            <Col lg={7} className={'mt-2'}>
              <Row className="align-items-center">
                <Col lg={{ span: 3, offset: 4 }} md={6}>
                  <div className="title-heading mt-4">
                    <h1 className="heading mb-2">Scanner.</h1>
                    <p className="para-desc ">Scanner.com vous permet d'effectuer en toute simplicité les traitements de vos factures!  </p>
                    <p className="para-desc ">
                    En utilisant Scanner, vous acceptez nos liens <a style={{color:"#ffb21f"}} href="/"> Politique de confidentialité,
                     Termes et Conditions</a> et <a style={{color:"#ffb21f"}} href>Accord de traitement de données</a>

                    </p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4}>
              <Nav tabs className="justify-content-center mt-3">
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "1"
                    })}
                    onClick={() => {
                      this.toggle("1")
                    }}
                  >
                    Connexion
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "2"
                    })}
                    onClick={() => {
                      this.toggle("2")
                    }}
                  >
                    Inscription
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.active}>
                <TabPane tabId="1">
                  <Login/>
                </TabPane>
                <TabPane tabId="2">
                  <Register/>
                </TabPane>
              </TabContent>
            </Col>
          </Row>
          
        </div>
        
    )
  }
}

export default Auth
