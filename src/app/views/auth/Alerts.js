import React, { Component, Fragment } from 'react';
import {connect} from "react-redux"
import {withAlert} from "react-alert";
import PropTypes from "prop-types"
import { error } from 'jquery';

export class Alerts extends Component {
    static propTypes={
        errorRg:PropTypes.object.isRequired,
      
        message:PropTypes.object.isRequired,
        
      }


      componentDidUpdate (prevProps){
          console.log(this.props.errorLg)
          const {errorRg, alert,message}= this.props;
          if (errorRg !== prevProps.errorRg  ){
              if(errorRg.msg.username) alert.error(`Pseudo:
              ${errorRg.msg.username.join()}`);
             
              if (errorRg.msg.password)  alert.error(`Password:
              ${errorRg.msg.password.join()}`);

              if (errorRg.msg.email)  alert.error(`Email:
              ${errorRg.msg.email.join()}`);

              //if (errorLg.msg.non_field_errors)  alert.error("nom d'utilisateur ou mot de passe incorrect!");

             // if (error.msg.message)  alert.error(`Notification;:
             // ${error.msg.message.join()}`);

             
              
            }
            
            
           
            if (message !==prevProps.message){
                if(message.userAdded) alert.success(message.userAdded);
                if(message.appAdded) alert.success(message.appAdded);
            }
          
        }


    render() {
        return <Fragment/>;
    }
}


 

const mapStateToProps=state=>({

    errorRg:state.auth.errorsRegister,
    message:state.auth.messageReducer 
  })

export default connect(mapStateToProps)( withAlert(Alerts));
