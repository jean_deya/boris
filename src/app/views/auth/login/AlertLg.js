import React, { Component, Fragment } from 'react';
import {connect} from "react-redux"
import {withAlert} from "react-alert";
import PropTypes from "prop-types"
import { error } from 'jquery';

export class Alerts extends Component {
    static propTypes={
        errorLg:PropTypes.object.isRequired,
        message:PropTypes.object.isRequired,
        
      }


      componentDidUpdate (prevProps){
          console.log(this.props.errorLg)
          const {errorLg, alert,message}= this.props;
          if (errorLg !==prevProps.errorLg ){
              if (errorLg.msg.non_field_errors)  alert.error("nom d'utilisateur ou mot de passe incorrect!");
            }
            
            
           
            if (message !==prevProps.message){
                if(message.userAuth) alert.success(message.userAuth) ;window.location.reload()
            }
          
        }


    render() {
        return <Fragment/>;
    }
}


 

const mapStateToProps=state=>({
    errorLg:state.auth.errorsLogin,
    message:state.auth.messageReducer 
  })

export default connect(mapStateToProps)( withAlert(Alerts));
