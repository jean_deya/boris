import React from "react"
import {Link, Redirect} from "react-router-dom"
import classnames from "classnames";
import AlertLg from "./AlertLg"
import {connect} from "react-redux"
import PropTypes from "prop-types"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"

import {loginWithToken} from "../../../@core/redux/actions/auth/loginActions"

import Checkbox from "../../../@vuexy/checkbox/CheckboxesVuexy"
import {Check, Mail, Lock} from "react-feather"

class Login extends React.Component {


  state = {
    username: "",
    password: "",
    remember: false,
    checkbox:"text"
  };

  static propTypes={

      loginWithToken:PropTypes.func.isRequired,
      isAuthenticated:PropTypes.bool
    }
  
  


  onChange=e=>{
    this.setState({
      [e.target.name]:e.target.value
    });
  }

 handleLogin = (e) => {
    e.preventDefault()
    console.log(this.state)
 // this.props.loginWithJWT(this.state)
 this.props.loginWithToken(this.state.username,this.state.password)
    
  };
  toggle = tab => {
    if (this.state.checkbox !== tab) {
      this.setState({checkbox: tab})
    }
  };

  

  render() {



   if (this.props.isAuthenticated){
     return <Redirect to ="/dashboard/ChoixProfile"/>;
    }
    return (
      <Card>
        <CardHeader>
        <AlertLg/>
          <CardTitle>Connexion</CardTitle>
        </CardHeader>
        <CardBody>
          <Form onSubmit={this.handleLogin}>
            <Row>
              <Col sm="12">
                <Label for="EmailVerticalIcons"> Nom d'utilisateur</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                   // type="email"
                    type="text"
                    name="username"
                    id="EmailVerticalIcons"
                    placeholder="Email / Nom d'utilisateur"
                    onChange={this.onChange}
                  />
                  <div className="form-control-position">
                    <Mail size={15}/>
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type={this.state.checkbox}
                    name="password"
                    id="IconsPassword"
                    placeholder="Mot de passe"
                    onChange={this.onChange}
                    className={classnames({
                      active: this.state.active === "password"
                    })}
                    onClick={() => {
                      this.toggle("password")
                    }}
                  />
                  <div className="form-control-position">
                    <Lock size={15}/>
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Checkbox
                    color="primary"
                    icon={<Check className="vx-icon" size={16}/>}
                    label="Se souvenir de moi"
                    defaultChecked={false}
                  />
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  >
                    Se connecter
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  return {
  
    isAuthenticated:state.auth.login.isAuthenticated
  }
};

export default connect(mapStateToProps, {loginWithToken})(Login)
