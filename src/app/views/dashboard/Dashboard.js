import React from "react"
import { Row, Col } from "reactstrap"
import SubscribersGained from "../../@core/components/cards/statistics/SubscriberGained"
import RevenueGenerated from "../../@core/components/cards/statistics/RevenueGenerated"
import QuaterlySales from "../../@core/components/cards/statistics/QuaterlySales"
import OrdersReceived from "../../@core/components/cards/statistics/OrdersReceived"
import RevenueChart from "../../@core/components/cards/analytics/Revenue"
import GoalOverview from "../../@core/components/cards/analytics/GoalOverview"
import BrowserStats from "../../@core/components/cards/analytics/BrowserStatistics"
import ClientRetention from "../../@core/components/cards/analytics/ClientRetention"
import SessionByDevice from "../../@core/components/cards/analytics/SessionByDevice"
import CustomersChart from "../../@core/components/cards/analytics/Customers"
import ChatWidget from "../../@vuexy/chatWidget/ChatWidget"

import "../../../assets/scss/plugins/charts/apex-charts.scss"

let $primary = "#7367F0",
  $success = "#28C76F",
  $danger = "#EA5455",
  $warning = "#FF9F43",
  $primary_light = "#9c8cfc",
  $warning_light = "#FFC085",
  $danger_light = "#f29292",
  $stroke_color = "#b9c3cd",
  $label_color = "#e7eef7"

class Dashboard extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
          <Col lg="3" md="6" sm="6">
            <SubscribersGained />
          </Col>
          <Col lg="3" md="6" sm="6">
            <RevenueGenerated />
          </Col>
          <Col lg="3" md="6" sm="6">
            <QuaterlySales />
          </Col>
          <Col lg="3" md="6" sm="6">
            <OrdersReceived />
          </Col>
        </Row>
        <Row className="match-height">
          <Col lg="4" md="6" sm="12">
            <BrowserStats />
          </Col>
          <Col lg="8" md="6" sm="12">
            <ClientRetention
              strokeColor={$stroke_color}
              primary={$primary}
              danger={$danger}
              labelColor={$label_color}
            />
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Dashboard