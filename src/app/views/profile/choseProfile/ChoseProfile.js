import React, { Component } from 'react'
import { Link} from "react-router-dom"
import {connect} from "react-redux"
import {getProfile} from "../../../@core/redux/actions/profileActions/profileActions"
import {
    Card,
    CardHeader,
    CardBody,
    Row,
    Col,
    Button,
    Alert
   
  } from "reactstrap"
  import {  Briefcase } from "react-feather"
import avatarImg from "../../../../assets/img/portrait/small/avatar-s-7.jpg"


export class ChoseProfile extends Component {

  componentDidMount(){
     this.props.getProfile()

   }
   

    render() {
      console.log("choseProfi",this.props.AllProfiles)
     
      //tous les profil
      const AllProfiles=this.props.AllProfiles

      
      const filterOwnProfiles=AllProfiles.filter(profile=>
      profile.entity===this.props.idUser && profile.users===this.props.idUser 
         )
      console.log('exactpro',filterOwnProfiles)

      console.log(filterOwnProfiles)
      const profiles=filterOwnProfiles.length ?(
        filterOwnProfiles.map((profile,index)=>{
          return(

              
                  <div key={index}>
                      <Row>
                      <Col lg="12"  sm="12">
                <Card>
                  <CardHeader className="mx-auto">
                    <div className="avatar mr-1 avatar-xl">
                      <img src={avatarImg} alt="avatarImg" />
                    </div>
                  </CardHeader>
                  <CardBody className="text-center">
              <h4>{this.props.username}</h4>
                    <p>Proprietaire</p>
                    <div className="card-btns d-flex justify-content-center">
                    
                      <Link onClick={()=>this.props.getProfile(profile.id)}   to={"/dashboard/ChoixProfile/" + profile.id}   color="primary" outline>
                        Choisir
                      </Link>
                    
                    </div>
                    <hr className="my-2" />
                    <div className="card-btns d-flex justify-content-between">
                      <div className="float-right">
                        <Briefcase size="15" className="primary" />
                        <span className="ml-50 align-middle">COMPTA-CI</span>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              
              
              </Col>
            </Row>
                  </div>
              )
              
            } 
        )
      ):
      
      (
        
        <Alert color="warning">
      { /*ChargemSent de votre <a href="#" className="alert-link"> profil</a>.
        */}</Alert>    
      )
//invite
const filterInvProfiles=AllProfiles.filter(profile=>
    profile.users===this.props.idUser && profile.is_first_user===false
     )
  console.log('exactpro',filterInvProfiles)

  console.log(filterOwnProfiles)
  const invite=filterInvProfiles.length ?(
    filterInvProfiles.map((profile,index)=>{
      return(

          
              <div key={index}>
                  <Row>
                  <Col lg="12"  sm="12">
            <Card>
              <CardHeader className="mx-auto">
                <div className="avatar mr-1 avatar-xl">
                  <img src={avatarImg} alt="avatarImg" />
                </div>
              </CardHeader>
              <CardBody className="text-center">
          <h4>{this.props.username}</h4>
                <p>INVITE</p>
                <div className="card-btns d-flex justify-content-center">
                
                  <Link onClick={()=>this.props.getProfile(profile.id)}   to={"/dashboard/ChoixProfile/" + profile.id}   color="primary" outline>
                    Choisir
                  </Link>
                
                </div>
                <hr className="my-2" />
                <div className="card-btns d-flex justify-content-between">
                  <div className="float-right">
                    <Briefcase size="15" className="primary" />
                    <span className="ml-50 align-middle">COMPTA-CI</span>
                  </div>
                </div>
              </CardBody>
            </Card>
          
          
          </Col>
        </Row>
              </div>
          )
          
        } 
    )
  ):
  
  (
    
    <Alert color="warning">
   { /*Vous n'avez encore créé aucun <a href="#" className="alert-link"> profil</a>.
    */}</Alert>    
  )


      return (<div>
    
  {profiles}
        {invite}
  </div>

  )

}
}

function mapDispatchToProps(){
  return{
    getProfile
  }
}

export default  connect(null,mapDispatchToProps())(ChoseProfile)
