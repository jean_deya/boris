import React from "react";
import {Nav, NavItem, NavLink, TabContent, TabPane, Row, Col} from "reactstrap";
import classnames from "classnames";
import ChoseProfile from "./choseProfile/ChoseProfile"
import CreateProfil from "./createprofile/CreateProfile"
import {getAllEntities} from "../../@core/redux/actions/auth/registerActions"
import { connect } from "react-redux";
import {getAllProfiles,getProfile} from "../../@core/redux/actions/profileActions/profileActions"
import { Info } from "react-feather";
//import Register from "./register/Register";
//import Login from "./login/Login";



class ChoixProfiles extends React.Component {
  state = {
    active: "1",
    info:false
  };

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  };

  
  componentWillMount(){
   // window.location.reload()
  }
  
  componentDidMount(){
    this.props.getAllEntities()
    this.props.getAllProfiles()

  }


  
  render() {
   
    console.log('prodile',this.props.AllProfiles)
    return (
      
        <div className={'mt-5'}>  
                      
          <Row className={'mr-1 ml-1'}>
            <Col lg={7} className={'mt-2'}>
              <Row className="align-items-center">
                <Col lg={{ span: 3, offset: 4 }} md={6}>
                  <div className="title-heading mt-4">
    <h1  className="heading mb-2">Bienvenue {this.props.username}</h1>
                    <p style={{fontWeight:"300px",fontSize:"16px"}} className="para-desc ">Veuillez choisir votre profil avant de continuer!</p>
                    <p style={{fontWeight:"300px",fontSize:"14px"}} className="para-desc ">
                      A quoi sert-il peut-être ?
                      <br/>
                      Eh bien !! Ce profil vous permettra d'accéder tout simplement à vos differents applications de travail.
                       
                      .</p>
                      <h5 style={{color:"#ffb21f"}} > l'attribution des tâches est plus simple et l'évolution du travail se fait en temps reel !!</h5>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4}>
              <Nav tabs className="justify-content-center mt-3">
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "1"
                    })}
                    onClick={() => {
                      this.toggle("1")
                    }}
                  >
                    Choisir un profil
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.active}>
               
                <TabPane tabId="1" >
                  <ChoseProfile entities={this.props.entities} username={this.props.username} idUser={this.props.idUser} AllProfiles={this.props.AllProfiles}/>
                </TabPane>
              </TabContent>
            </Col>
          </Row>
          
        </div>
        
    )
  }
}
const mapStateToProps = state => {
  return {
    idUser:state.auth.login.user.id,
    username:state.auth.login.user.username,
    entities:state.auth.EntitiesReducer.entities,
    AllProfiles:state.profileReducer.profiles,
   
        isAuthenticated:state.auth.login.isAuthenticated
    
  
  }
};

export default connect(mapStateToProps,{getAllProfiles,getAllEntities,getProfile})(ChoixProfiles)
