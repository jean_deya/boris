import React from "react"
import {Link, Redirect} from "react-router-dom"
import {addProfile} from "../../../@core/redux/actions/profileActions/profileActions"
import {connect} from "react-redux"
import PropTypes from "prop-types"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"
import {User, Mail,Lock, Coffee } from "react-feather"
class CreateProfile extends React.Component {

  state = {
    profil:{
        id:"",
        key:"",
        secret:"",
        entity:"",
        users:"",
        is_first_user:false
        },
}

  static propTypes={

     
      isAuthenticated:PropTypes.bool
    }
  
  
  changeHandler=e=>{
    this.setState({
      profil:{
        ...this.state.profil,
        [e.target.name]:e.target.value,
        id:this.props.username ,
        is_first_user:true,
        entity:this.props.username,
        users:this.props.idUser
        }
      
    });
  }


 handleSubmit = (e) => {
    e.preventDefault()
    console.log(this.state)
   // this.props.loginWithJWT(this.state)
   if(
  this.props.addProfile(this.state.profil)
  ){

  }

  };
 
  

  render() {
console.log(this.props.username)
    return (
      <Card>
        <CardHeader>
          <CardTitle>Ajouter un profile</CardTitle>
        </CardHeader>
        <CardBody>
            <form onSubmit={this.handleSubmit}>
            <Row>
              <Col sm="12">
                <Label for="EmailVerticalIcons"> Nom profile</Label>
                <FormGroup className="has-icon-left position-relative">
                <Input
                isRequired
                    type="text"
                    name="secret"
                    id="key"
                    placeholder="Nom"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
            
            
              <Col sm="12">
                <Label for="EmailVerticalIcons"> prenom profil</Label>
                <FormGroup className="has-icon-left position-relative">
                <Input
                    isRequired
                    type="text"
                    name="key"
                    id=""
                    placeholder="prenom profile"
                    onChange={this.changeHandler}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  //  onClick={e => e.preventDefault()}
                  >
                    AJouter ce profile
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
            </form>
        </CardBody>
      </Card>
    )
  }
}


export default connect(null,{addProfile})(CreateProfile)
