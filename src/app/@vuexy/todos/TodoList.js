import React from "react"
import {FormGroup, Input} from "reactstrap"
import {Menu, Search, Maximize} from "react-feather"
import {connect} from "react-redux"
import {
  getTodos,
  completeTask,
  starTask,
  importantTask,
  trashTask,
  searchTask
} from "../../@core/redux/actions/todo/index"
import Row from "reactstrap/es/Row";
import Col from "reactstrap/es/Col";
import DataTableBasic from "./DataTableBasic";
import {Link} from "react-router-dom"


class TodoList extends React.Component {
 
  state = {
    file:"",
    todos: [],
    handleUpdateTask: null,
    currentLocation: this.props.routerProps.location.pathname,
    value: "",
    clic:this.props.data
  }



  handleOnChange = e => {
    this.setState({value: e.target.value})
    this.props.searchTask(e.target.value)
  };


  render() {

    //


    const {value} = this.state;
    const{todos}= this.props
    
    const todoPost=todos.length ?(
      todos.map(todo=>{
        if(todo.id==this.props.data.id){ 
          return( 
            <Col key={todo.id} lg={6}>
          
            
            <div style={{textAlign: 'right'}}>
              <Link to={"/dashboard/apps/:appId/ZoomDoc/" + todo.id}>
              <Maximize />
              </Link>
            </div>
            <img width={250} style={{
              objectFit: 'contain',
              maxWidth: '100%',
              margin: 'auto',
              display: 'block'
            }}
                 src={todo.image} alt={'Document vide'}/>
          </Col>
         );
        }
      })
      
      
      ):( 
        <p>SECETIONNNER</p>

);

    return (
      <div className="content-right">
        <div className="todo-app-area">
          <div className="todo-app-list-wrapper">
            <div className="todo-app-list">
              <div className="app-fixed-search">
                <div
                  className="sidebar-toggle d-inline-block d-lg-none"
                  onClick={() => this.props.mainSidebar(true)}
                >
                  <Menu size={24}/>
                </div>
                <FormGroup className="position-relative has-icon-left m-0 d-inline-block d-lg-block">
                  <Input
                    type="text"
                    placeholder="Rechercher..."
                    onChange={e => this.handleOnChange(e)}
                    value={value}
                  />
                  <div className="form-control-position">
                    <Search size={15}/>
                  </div>
                </FormGroup>
              </div>
              <Row>
               {todoPost}
                <Col lg={6}>
                  <DataTableBasic/>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
    )
  }
}



const mapStateToProps =(state) => {
    //faire correspondre l id de notre article de blogue a l article du reducer
  return{
    app:state.todoApp,
    todos:state.todoApp.todo.todos
     //retourner un seul post

  };
}
export default connect(mapStateToProps, {
  getTodos,
  completeTask,
  starTask,
  importantTask,
  trashTask,
  searchTask
})(TodoList)
