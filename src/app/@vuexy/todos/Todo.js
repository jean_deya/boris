import React from "react"
import Sidebar from "react-sidebar"
import { ContextLayout } from "../../@core/utility/context/Layout"
import TodoSidebar from "./TodoSidebar"
import TodoList from "./TodoList"
import TaskSidebar from "./TaskSidebar"
import "../../../assets/scss/pages/app-todo.scss"
import axios from"axios"
const mql = window.matchMedia(`(min-width: 992px)`);

class TODO extends React.Component {


  state = {
    file:[],
    addTask: false,
    sidebarDocked: mql.matches,
    sidebarOpen: false,
    taskToUpdate: null,
    prevState: null
  }
  componentDidMount() {  
  axios 
  .get("http://127.0.0.1:8080/v1/files/")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
  .then(res=>{
    console.log("info dashboard",res.data.results)
    this.setState({
     file:res.data.results
 
    });
  })
  }
  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  mediaQueryChanged = () => {
    this.setState({ sidebarDocked: mql.matches, sidebarOpen: false })
  }

  handleAddTask = status => {
    status === "open"
      ? this.setState({ addTask: true })
      : this.setState({ addTask: false, taskToUpdate: null })
  }
  handleUpdateTask = todo => {
    if (todo !== undefined) {
      this.setState({ addTask: true, taskToUpdate: todo })
    } else {
      this.setState({ taskToUpdate: null })
    }
  }

  handleUndoChanges = arr => {
    this.setState({
      prevState: arr
    })
  }

  render() {
    return (
      <div className="todo-application position-relative">
        <div
          className={`app-content-overlay ${
            this.state.addTask || this.state.sidebarOpen ? "show" : ""
          }`}
          onClick={() => {
            this.handleAddTask("close")
            this.onSetSidebarOpen(false)
          }}
        />
        
        <TodoList
          routerProps={this.props}
          handleUpdateTask={this.handleUpdateTask}
          mainSidebar={this.onSetSidebarOpen}
          prevState={this.state.prevState}
        />
        <TaskSidebar
          addTask={this.handleAddTask}
          addTaskState={this.state.addTask}
          taskToUpdate={this.state.taskToUpdate}
          newTask={this.state.newTask}
          mainSidebar={this.onSetSidebarOpen}
          handleUndoChanges={this.handleUndoChanges}
        />
      </div>
    )
  }
}

export default TODO
