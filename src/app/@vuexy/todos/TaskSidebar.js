import React from "react"
import axios from "axios"
import {
  Input,
  Button,
  FormGroup,
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from "reactstrap"
import {X, Info, Star, Tag, Check} from "react-feather"
import Checkbox from "../checkbox/CheckboxesVuexy"
import PerfectScrollbar from "react-perfect-scrollbar"
import {connect} from "react-redux"
import {
  starTask,
  completeTask,
  importantTask,
  updateTask,
  updateLabel,
  addNewTask
} from "../../@core/redux/actions/todo/index"
import {Link, Redirect} from 'react-router-dom';
import "./index.css"
import {mok} from "../../@core/@fake-db/mock.js"

class TaskSidebar extends React.Component {
  state = {
    taskToUpdate: null,//mise a jour tache m,
    taskTitle: "",
    taskDesc: "",
    fileImport:false,
    Imagen:"",
    taskStatus: false,
    taskStarred: false,
    taskImportant: false,
    newTask: {
      application: "",
      desc: "",
      image:"",

      tags: [],
      isCompleted: false,
      isImportant: false,
      isStarred: false
    }
  };

  //explication pas claire
  componentDidUpdate(prevProps, prevState) {
    if (
      (this.props.taskToUpdate !== null && this.state.taskToUpdate !== this.props.taskToUpdate) ||
      (this.props.taskToUpdate !== null && this.state.taskStatus !== this.props.taskToUpdate.isCompleted) ||
      (this.props.taskToUpdate !== null && this.state.taskStarred !== this.props.taskToUpdate.isStarred) ||
      (this.props.taskToUpdate !== null && this.state.taskImportant !== this.props.taskToUpdate.isImportant)
    ) {
      this.setState({
        taskToUpdate: this.props.taskToUpdate,
        taskTitle: this.props.taskToUpdate.title,
        file:this.props.taskToUpdate.image,
        taskDesc: this.props.taskToUpdate.desc,
        taskStatus: this.props.taskToUpdate.isCompleted,
        taskStarred: this.props.taskToUpdate.isStarred,
        taskImportant: this.props.taskToUpdate.isImportant
      })
    } 

  };

  handleNewTaskTags = tag => {
    let tagsArr = this.state.newTask.tags
    if (tagsArr.includes(tag)) {
      tagsArr.splice(tagsArr.indexOf(tag), 1)
    } else {
      tagsArr.push(tag)
    }
    this.setState({
      ...this.state.newTask,
      tags: tag
    })
  };



  //lire l'image
  imageHandler =(e)=>{
    if (this.props.taskToUpdate !== null) {
      
      this.setState({
        file: e.target.files[0],
        Imagen:this.state.taskImage
      })
    } else {
      this.setState({
        newTask: {
          ...this.state.newTask,
          
          file: e.target.files[0]
        }
      })
    }
    const reader = new FileReader();
    reader.onload=()=>{
        if (reader.readyState ===2 && this.props.taskToUpdate !== null){
          this.setState({ file: reader.result});
            }else if(reader.readyState ===2 && this.props.taskToUpdate === null){
              this.setState({
                newTask: {
                  ...this.state.newTask,
                  image:reader.result
                }
              })}
    };
    reader.readAsDataURL(e.target.files[0]);
};
 //Mettre les informations dans la console 
 
 submit=e=>{
 // e.preventDefault();
  console.log("fdfd ",this.state.newTask.file)
  const fileform=new FormData();
  fileform.append('file',this.state.newTask.file);
  
  console.log("factrue ",fileform)
  axios.post("http://127.0.0.1:8080/v1/files/",fileform)
  .then(response=>{
    if(response.data){
      this.setState({
        fileImport:true
      })
    }
  })
  
}




  render() {
    const {taskToUpdate, taskTitle, taskDesc, newTask, taskStatus} = this.state;
    if (this.state.fileImport===true){
      return <Redirect to="/dashboard/apps/:appId/documents/"/>;
    }
    return (
      <div
        className={`task-sidebar ${
          this.props.addTaskState === true ? "show" : ""
        }`}
      >
        <div className="task-header">
          <div className="d-flex justify-content-between">
            <div className="task-type-title text-bold-600">
              <h3>
                {this.props.taskToUpdate && this.props.taskToUpdate.id //PAS CLAIRE
                  ? "Update Task"
                  : "importer facture"}
              </h3>
            </div>
            <div className="close-icon">
              <X
                className="cursor-pointer"
                size={20}
                onClick={() => this.props.addTask("close")}
              />
            </div>
          </div>
        </div>

        
        <PerfectScrollbar>
        <form onSubmit={this.submit}>

          <div className="task-body">
            <div className="d-flex justify-content-between mb-2">
              <div className="mark-complete">
                {this.props.taskToUpdate && this.props.taskToUpdate.id && <Checkbox
                  color="primary"
                  className="user-checkbox"
                  icon={<Check className="vx-icon" size={15}/>}
                  label={""}
                  checked={taskStatus}
                  onChange={e => {
                    this.props.completeTask(this.props.taskToUpdate)
                  }}
                />}
              </div>
              <div className="task-actions">
                <Info
                  size={20}
                  className={`mr-50 ${
                    (this.props.taskToUpdate !== null && this.state.taskImportant) ||
                    newTask.isImportant
                      ? "text-success"
                      : ""
                  }`}
                  onClick={() => {
                    if (this.props.taskToUpdate !== null) {
                      this.props.importantTask(this.props.taskToUpdate)
                    } else {
                      this.setState({
                        newTask: {
                          ...this.state.newTask,
                          isImportant: !this.state.newTask.isImportant
                        }
                      })
                    }

                  }}
                />
                <Star
                  size={20}
                  className={`mr-50 ${
                    (this.props.taskToUpdate !== null && this.state.taskStarred) ||
                    newTask.isStarred
                      ? "text-warning"
                      : ""
                  }`}
                  onClick={() => {
                    if (this.props.taskToUpdate !== null) {
                      this.props.starTask(this.props.taskToUpdate)
                    } else {
                      this.setState({
                        newTask: {
                          ...this.state.newTask,
                          isStarred: !this.state.newTask.isStarred
                        }
                      })
                    }

                  }}
                />
                <UncontrolledDropdown className="d-inline-block">
                  <DropdownToggle tag="span">
                    <Tag className="mr-50" size={20}/>
                  </DropdownToggle>
                  <DropdownMenu tag="ul" right>
                    <DropdownItem tag="li">
                      <Checkbox
                        color="primary"
                        className="user-checkbox"
                        icon={<Check className="vx-icon" size={12}/>}
                        label={"Frontend"}
                        checked={
                          (this.props.taskToUpdate !== null &&
                            this.props.taskToUpdate.tags.includes("frontend")) ||
                          (newTask.tags.includes("frontend"))
                            ? true
                            : false
                        }
                        size="sm"
                        onChange={e => {
                          e.stopPropagation()
                          if (this.props.taskToUpdate !== null)
                            this.props.updateLabel(taskToUpdate.id, "frontend")
                          else this.handleNewTaskTags("frontend")
                        }}
                      />
                    </DropdownItem>
                    <DropdownItem tag="li">
                      <Checkbox
                        color="primary"
                        className="user-checkbox"
                        icon={<Check className="vx-icon" size={12}/>}
                        label={"Backend"}
                        checked={
                          (this.props.taskToUpdate !== null &&
                            this.props.taskToUpdate.tags.includes("backend")) ||
                          (newTask.tags.includes("backend"))
                            ? true
                            : false
                        }
                        size="sm"
                        onClick={e => e.stopPropagation()}
                        onChange={e => {
                          e.stopPropagation()
                          if (this.props.taskToUpdate !== null)
                            this.props.updateLabel(taskToUpdate.id, "backend")
                          else this.handleNewTaskTags("backend")
                        }}
                      />
                    </DropdownItem>
                    <DropdownItem tag="li">
                      <Checkbox
                        color="primary"
                        className="user-checkbox"
                        icon={<Check className="vx-icon" size={12}/>}
                        label={"Doc"}
                        checked={
                          (this.props.taskToUpdate !== null &&
                            this.props.taskToUpdate.tags.includes("doc")) ||
                          (newTask.tags.includes("doc"))
                            ? true
                            : false
                        }
                        size="sm"
                        onClick={e => e.stopPropagation()}
                        onChange={e => {
                          e.stopPropagation()
                          if (this.props.taskToUpdate !== null)
                            this.props.updateLabel(taskToUpdate.id, "doc")
                          else this.handleNewTaskTags("doc")
                        }}
                      />
                    </DropdownItem>
                    <DropdownItem tag="li">
                      <Checkbox
                        color="primary"
                        className="user-checkbox"
                        icon={<Check className="vx-icon" size={12}/>}
                        label={"Bug"}
                        checked={
                          (this.props.taskToUpdate !== null &&
                            this.props.taskToUpdate.tags.includes("bug")) ||
                          (newTask.tags.includes("bug"))
                            ? true
                            : false
                        }
                        size="sm"
                        onClick={e => e.stopPropagation()}
                        onChange={e => {
                          e.stopPropagation()
                          if (this.props.taskToUpdate !== null)
                            this.props.updateLabel(taskToUpdate.id, "bug")
                          else this.handleNewTaskTags("bug")
                        }}
                      />
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </div>
            </div>
            
            <FormGroup>    
          
            <input  
                  type="file"
                  name="image"
                   id="input" 
                   accept="Image/*"
                    onChange={this.imageHandler}/>
                 
                  <div className="label">
                      <img width={70} height={50} src={newTask.image} alt="" id="img" className="img"/>
                        <label htmlFor="input" className="image-upload">
                            <i className="material-icons">add_photo_alternate</i>
                            Choisir la facture
                        </label>
                    </div>
            </FormGroup>

            <FormGroup>
              <Input
                type="text"
                placeholder="Title"
                value={
                  this.props.taskToUpdate !== null ? taskTitle : newTask.title
                }
                onChange={e => {
                  if (this.props.taskToUpdate !== null) {
                    this.setState({
                      taskTitle: e.target.value
                    })
                  } else {
                    this.setState({
                      newTask: {
                        ...this.state.newTask,
                        application: e.target.value
                      }
                    })
                  }
                }}
              />
            </FormGroup>
            <FormGroup>
              <Input
                type="textarea"
                placeholder="Description"
                rows="5"
                value={
                  this.props.taskToUpdate !== null ? taskDesc : newTask.desc
                }
                onChange={e => {
                  if (this.props.taskToUpdate !== null) {
                    this.setState({
                      taskDesc: e.target.value
                     })
                  } else {
                    this.setState({
                      newTask: {
                        ...this.state.newTask,
                        desc: e.target.value
                      }
                    })
                  }
                }}
              />
            </FormGroup>
            <div className="chip-wrapper my-1 d-flex flex-wrap">
              {this.props.taskToUpdate !== null &&
              this.props.taskToUpdate.tags &&
              this.props.taskToUpdate.tags.length > 0
                ? this.renderTags(this.props.taskToUpdate.tags)
                : null}
            </div>

            <hr className="my-2"/>
            <div className="d-flex justify-content-end">
              <Button.Ripple
                className="mr-50"
                color="primary"
                
              >
               
                   
                  "Add"
              </Button.Ripple>
              <Button.Ripple
                color="light"
                outline
                onClick={() => {
                  this.props.addTask("close")
                  this.setState({
                    newTask: {
                      application: "",
                      desc: "",
                      image:"",
                      tags: [],
                      isCompleted: false,
                      isImportant: false,
                      isStarred: false
                    }
                  })
                }}
              >
                Cancel
              </Button.Ripple>
            </div>
          </div>
          </form>
        </PerfectScrollbar>
       

      </div>
    )
  };
}
//EXPLIQUATION

const mapStateToProps = state => {
  return {
    app: state.todoApp
  }
};

export default connect(mapStateToProps, {
  completeTask,
  importantTask,
  starTask,
  updateTask,
  updateLabel,
  addNewTask
})(TaskSidebar)
