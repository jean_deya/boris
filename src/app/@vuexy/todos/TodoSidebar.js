import React from "react"
import {Button, ListGroup, ListGroupItem} from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import {X} from "react-feather"
import {connect} from "react-redux"
import {changeFilter} from "../../@core/redux/actions/todo/index"
import{Link} from "react-router-dom"
import axios from  "axios"

class TodoSidebar extends React.Component {

  static getDerivedStateFromProps(props, state) {
    if (props.app.todo.routeParam !== state.currentLocation) {
      return {
        todos: props.app.todo.todos
      }
    }
    // Return null if the state hasn't changed
    return null
  }

  state = {
    
    todos: [],
    handleUpdateTask: null,
    currentLocation: this.props.routerProps.location.pathname,
    value: ""
  }
  

 componentDidMount() {    
  this.setState({
      todos: this.props.app.todo.todos,
      handleUpdateTask: this.props.handleUpdateTask,
    })

}
 

 



  render() {
    
        //
        console.log("files ",this.props.file[0])
        const{file}=this.props
    
   
    const{todos}= this.props
   // console.log('ste',this.state.todos)
  //  console.log('app',todos)
    const filePost=file.length ?(
      file.map((files,index)=>{
        return(
          <ListGroupItem
          key={index}
                className="border-0"
            >
            <Link to={"/dashboard/apps/:appId/documents/" +files.id}>
              <Button onClick={()=>this.props.data.ajout(files.id)}> 
              <img width={'200px'} src={files.file}alt={''}/>
              </Button>
            </Link> 
          </ListGroupItem>
          )
        })
):( 
          <p>Aucun documents</p>
);
    return (
      <React.Fragment>
        <span
          className="sidebar-close-icon"
          onClick={() => this.props.mainSidebar(false)}
        >
          <X size={15}/>
        </span>
        <div className="todo-app-menu">
          <div className="add-task">
    
            <Button.Ripple
              block
              className="btn-block my-1"
              color="primary"
              onClick={() => {
                this.props.addTask("open");
                this.props.mainSidebar(false);
              }}
            >
              Nouveau document
            </Button.Ripple>
          </div>
          <PerfectScrollbar
            className="sidebar-menu-list"
            options={{
              wheelPropagation: false
            }}
          >
            <ListGroup className="font-medium-1">
             {console.log("forf",filePost)}
              {filePost}
              </ListGroup>
            <hr/>
          </PerfectScrollbar>
        </div>
      </React.Fragment>
    )
  }
}


const mapStateToProps = state => {
  return {
    app:state.todoApp,
    todos: state.todoApp.todo.todos
 }
}



export default connect(mapStateToProps, {changeFilter})(TodoSidebar)
