import React from "react"
import DataTable from "react-data-table-component"

const columns = [
  {
    name: "clé",
    selector: "key",
    sortable: true
  },
  {
    name: "Libellé",
    selector: "Label",
    sortable: true
  },
  {
    name: "Valeur",
    selector: "Value",
    sortable: true
  }
];

const data = [
  {
    id: 1,
    key: "TVA",
    last_name: "Lillecrop"
  },
  {
    id: 2,
    key: "Date",
    last_name: "Pentlow"
  },
  {
    id: 3,
    key: "Fournisseur",
    last_name: "Morley"
  },
  {
    id: 4,
    key: "Prix catalogue",
    last_name: "Jerrard"
  },
  {
    id: 5,
    first_name: "Numérotation de la facture",
    last_name: "Plose"
  },
];

class DataTableBasic extends React.Component {
  render() {
    return (
      <DataTable data={data} columns={columns} noHeader />
    )
  }
}

export default DataTableBasic
