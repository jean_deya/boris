import React, { useState } from "react"
import {connect} from "react-redux"
import "../../../../../assets/css/NavBarUser.css"


import {
  CardHeader,
  Button,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Label
} from "reactstrap"
import axios from "axios"
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import {User, Mail,Lock, Coffee } from "react-feather"
import avatarImg from "../../../../../assets/img/portrait/small/avatar-s-12.jpg"


const EditProfil = props => {
    const {
      className,username,toggle,modal
    } = props;
  
 
    const [nestedModal, setNestedModal] = useState(false);
    const [closeAll, setCloseAll] = useState(false);
  
   
    const toggleNested = () => {
      setNestedModal(!nestedModal);
      setCloseAll(false);
    }
    const toggleAll = () => {
      setNestedModal(!nestedModal);
      setCloseAll(true);
    }
  


    return (
      <div>
        <Modal isOpen={modal} toggle={toggle} className={className}>
          <ModalHeader toggle={toggle}>Modification des informations de votre profil</ModalHeader>
          <ModalBody>
          <CardBody>
            
            <form >
                <Row>
             
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Nom</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="first_name"
                        placeholder="BOBO "
                        
                       
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Prenom</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="last_name"
                        placeholder="yvan"
                       
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Pseudo</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="username"
                        placeholder="bobo45"
                       
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Nom Entreprise</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="company_name"
                        placeholder="COMPTA-CI"
                        
                      />
                      <div className="form-control-position">
                        <Coffee size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                
                  <Col sm="12">
                    <Label for="IconsPassword">Mot de passe</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="password"
                        name="password"
                        id="password"
                        placeholder="Mot de passe"
                        
                      />
                      <div className="form-control-position">
                        <Lock size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                  <FormGroup className="has-icon-left position-relative">
                  <Button color="success" onClick={toggleNested}>photo</Button>
          <Modal isOpen={nestedModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined}>
            <ModalHeader>modifier photo de profile</ModalHeader>
            <ModalBody>
            <CardHeader className="mx-auto">
              <div className="avatar mr-1 avatar-xl">
                <img src={avatarImg} alt="avatarImg" />
              </div>
            </CardHeader>


            </ModalBody>
            <Button color="primary" onClick={toggleNested}>modifier</Button>
            </Modal>
            </FormGroup>
            </Col>
                  <Col sm="12">
                    <FormGroup className="has-icon-left position-relative">
                      <Button.Ripple
                        color="primary"
                        type="submit"
                        className="mr-1 mb-1"
                      //  onClick={e => e.preventDefault()}
                      >
                        modifier
                      </Button.Ripple>
                    </FormGroup>
                  </Col>
                 
                </Row>
              </form>
            </CardBody>

             
        </ModalBody>
        <ModalFooter>
        
          <Button color="secondary" onClick={toggle}>Retour</Button>
        </ModalFooter>
      </Modal>




    </div>

  )
}

  
  
const mapStateToProps= (state)=>{
    return{
      username:state.auth.login.user.username,
      lastname:state.auth.login.user.last_name,
      first_name:state.auth.login.user.first_name,
      isAuthenticated:state.auth.login.isAuthenticated
     
    }
  }
  
  export default connect(mapStateToProps)(EditProfil)