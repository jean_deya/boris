import React from "react"
import { Navbar } from "reactstrap"
import classnames from "classnames"
import NavbarUser from "./NavbarUser"
import Alerts from "../../../../views/auth/Alerts"

import {getAllProfiles} from "../../../redux/actions/profileActions/profileActions"

import {connect} from "react-redux"
import { combineReducers } from "redux"

//document.location.reload(false)

const ThemeNavbar = props => {
  
  console.log("dd",props.AllProfiles)
  const colorsArr = [ "primary", "danger", "success", "info", "warning", "dark"]
  const navbarTypes = ["floating" , "static" , "sticky" , "hidden"]
  return (
  
    <React.Fragment>
      <div className="content-overlay" />
      <div className="header-navbar-shadow" />
      <Navbar
        className={classnames(
          "header-navbar navbar-expand-lg navbar navbar-with-menu navbar-shadow",
          {
            "navbar-light": props.navbarColor === "default" || !colorsArr.includes(props.navbarColor),
            "navbar-dark": colorsArr.includes(props.navbarColor),
            "bg-primary":
              props.navbarColor === "primary" && props.navbarType !== "static",
            "bg-danger":
              props.navbarColor === "danger" && props.navbarType !== "static",
            "bg-success":
              props.navbarColor === "success" && props.navbarType !== "static",
            "bg-info":
              props.navbarColor === "info" && props.navbarType !== "static",
            "bg-warning":
              props.navbarColor === "warning" && props.navbarType !== "static",
            "bg-dark":
              props.navbarColor === "dark" && props.navbarType !== "static",
            "d-none": props.navbarType === "hidden" && !props.horizontal,
            "floating-nav":
              (props.navbarType === "floating" && !props.horizontal) || (!navbarTypes.includes(props.navbarType) && !props.horizontal),
            "navbar-static-top":
              props.navbarType === "static" && !props.horizontal,
            "fixed-top": props.navbarType === "sticky" || props.horizontal,
            "scrolling": props.horizontal && props.scrolling

          }
        )}
      >
        <Alerts/>
        <div className="navbar-wrapper">
          <div className="navbar-container content">
            <div
              className="navbar-collapse d-flex justify-content-between align-items-center"
              id="navbar-mobile"
            >
              {props.horizontal ?
                null
                : <div className="bookmark-wrapper" />
              }

              {props.horizontal ? (
                <div className="logo d-flex align-items-center">
                  <h2 className="text-primary brand-text mb-0">Scanner.</h2>
                </div>
              ) : null}

              {props.displayUser  ? (<NavbarUser
                handleAppOverlay={props.handleAppOverlay}
                changeCurrentLang={props.changeCurrentLang}
               // userName={props.username.username}
                //userImg={ userImg }
              />) : null}

            </div>
          </div>
        </div>
      </Navbar>
    </React.Fragment>
  )
}


const mapStateToProps= (state)=>{
  return{
    profile:state.profileReducer.profile,
    //afficher les profiles
    AllProfiles:state.profileReducer.profiles,

   
  }
}


export default connect(mapStateToProps,{getAllProfiles})(ThemeNavbar)

