import React, { useState } from "react"
import {connect} from "react-redux"
import "../../../../../assets/css/NavBarUser.css"
import {
  Button,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Label, Alert
} from "reactstrap"
import axios from "axios"
import { Modal, ModalHeader, ModalBody} from 'reactstrap'
import {User, Mail,Lock, Coffee } from "react-feather"
import {addCollaborator} from "../../../redux/actions/collaActions/collaboratorActions"
import Alerts from "../../../../views/auth/Alerts"

class  AddProfil  extends React.Component {


state={
  user:{
    first_name:"",
    last_name:"",
    username:"",
    email:"",
    password:"",
    created_at: new Date().toLocaleDateString("en-CA"),
    updated_at:"",
    created_by:"",
    updated_by:"",
    company_name:"yeao et frere",
  },
  profile:{
    
    entity:"1",
    users:"1",
    is_first_user:false
    }

  }

  changeHandler=e=>{
    
    this.setState({
      user: {
        ...this.state.user,
      [e.target.id]:e.target.value,
      created_at:this.state.user.created_at,
      created_by:this.state.user.username,
      updated_at:this.state.user.created_at,
      updated_by:this.state.user.username,
      

      },
      
     
    })
  }

 handleSubmit=e=>{
   console.log(this.state)
  this.props.addCollaborator(this.state)
 // e.preventDefault()

 }

  
  render(){
    const {
      className,username,toggle2,modal2
    } =this.props;
  



  
    return (
      <div>
        <Modal isOpen={modal2} toggle={toggle2} className={className}>
          <ModalHeader toggle={toggle2}>Ajouter un collaborateur </ModalHeader>
          <ModalBody>
          <CardBody>
            <Alerts/>
            <form onSubmit={this.handleSubmit}>
                <Row>
             
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Nom</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="first_name"
                        placeholder="nom "
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Prenom</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="last_name"
                        placeholder="Prenom"
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Pseudo</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="username"
                        placeholder="pseudo"
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <User size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="nameVerticalIcons">Nom Entreprise</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="text"
                        name="name"
                        id="company_name"
                        placeholder="COMPTA-CI"
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <Coffee size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                 
                    <Label for="EmailVerticalIcons">Email</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="email"
                        name="Email"
                        id="email"
                        placeholder="Email"
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <Mail size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                  <Col sm="12">
                    <Label for="IconsPassword">Mot de passe</Label>
                    <FormGroup className="has-icon-left position-relative">
                      <Input
                        type="password"
                        name="password"
                        id="password"
                        placeholder="Mot de passe"
                        onChange={this.changeHandler}
                      />
                      <div className="form-control-position">
                        <Lock size={15} />
                      </div>
                    </FormGroup>
                  </Col>
                
                  <Col sm="12">
                    <FormGroup className="has-icon-left position-relative">
                      <Button.Ripple
                        color="primary"
                        type="submit"
                        className="mr-1 mb-1"
                      //  onClick={e => e.preventDefault()}
                      >
                        ajouter ce collaborateur
                      </Button.Ripple>
                    </FormGroup>
                    <Button color="secondary" onClick={toggle2}>Retour</Button>
                  </Col>
                 
                </Row>
              </form>
            </CardBody>
        </ModalBody>
      </Modal>




    </div>

  )
}
}
  
  
const mapStateToProps= (state)=>{
    return{
      username:state.auth.login.user.username,
      lastname:state.auth.login.user.last_name,
      first_name:state.auth.login.user.first_name,
      isAuthenticated:state.auth.login.isAuthenticated
     
    }
  }
  
  export default connect(mapStateToProps,{addCollaborator})(AddProfil)