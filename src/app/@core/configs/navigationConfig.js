import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "dashboard",
    title: "Tableau de bords",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/dashboard"
  },
  {
    id: "documents",
    title: "Documents",
    type: "item",
    icon: <Icon.Layers size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/documents"
  },
  {
    id: "api_keys",
    title: "Api Keys",
    type: "item",
    icon: <Icon.Settings size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/settings",
    disabled: true
  },
  {
    id: "billing",
    title: "Facturation",
    type: "item",
    icon: <Icon.ShoppingBag size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/billing",
    disabled: true
  },
  {
    id: "Documentation",
    title: "Documentation",
    type: "item",
    icon: <Icon.Coffee size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/apps/:appId/documentation",
    disabled: true
  }
];

export default navigationConfig
