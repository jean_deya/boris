import { combineReducers } from "redux"
import { login } from "./loginReducer"
import { register } from "./registerReducers"
import { ProfilesReducer } from "./ProfilesReducer"
import {EntitiesReducer} from "./EntitiesReducer"
import { errorsLogin } from "./errorLgReducer"
import {errorsRegister } from "./errorRegReducer"
import {messageReducer} from "./messageReducer"
const authReducers = combineReducers({
  login,
  register,
  ProfilesReducer,
  EntitiesReducer,
  errorsLogin,
  errorsRegister,
  messageReducer
  
 
});

export default authReducers
