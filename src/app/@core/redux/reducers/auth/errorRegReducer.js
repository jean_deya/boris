//initialisation utilisateur
const initialState={
    msg:{},
    status:null
   
  };
  
  export const errorsRegister= (state = initialState, action) => {
    switch (action.type) {
        case "GET_ERRORS_REGISTER":
            return{
              msg:action.payload.msg,
              status:action.payload.status
  
            }
      default: {
        return state
      }
    }
  }
  