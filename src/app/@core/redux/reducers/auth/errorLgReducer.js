//initialisation utilisateur
const initialState={
    msg:{},
    status:null
   
  };
  
  export const errorsLogin= (state = initialState, action) => {
    switch (action.type) {
        case "GET_ERRORS_LOGIN":
            return{
              msg:action.payload.msg,
              status:action.payload.status
  
            }
      default: {
        return state
      }
    }
  }
  