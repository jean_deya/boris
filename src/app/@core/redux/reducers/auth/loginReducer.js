import { returnError } from "../../actions/auth/messageAction"

export const login = (
  state = {
    userRole: "admin",
    auth_token:localStorage.getItem("auth_token"),
    //isAuthenticated: !!localStorage.getItem('access_token')
    isAuthenticated:null,
    isLoading:false,
    user:{ },
  

  }, action) => {
  switch (action.type) {
    case "USER_LOADING":
      return{
        ...state,
        isLoading:true,
      
      }
      case "USER_LOADED":
       
        return{
          ...state,
          isAuthenticated:true,
          isLoading:false,
          user:action.payload
        }
      case "LOGIN_SUCCESS":
       localStorage.setItem("auth_token",
       action.payload.auth_token)
       
       ;
       return{
         ...state,
         ...action.payload,
         user:action.payload,
         isAuthenticated:true,
         isLoading:false
       }
        case "AUTH_ERROR":
          case "LOGIN_FAIL":
            case "LOGOUT_SUCCESS":
        localStorage.removeItem("auth_token")
        return{
          ...state,
          auth_token:null,
          user:null,
          isAuthenticated:false,
          isLoading:false
        }

       
        
    default: {
      return state
    }
  }
}
