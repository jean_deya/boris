import { returnError } from "../../actions/auth/messageAction"

export const EntitiesReducer = (
  state = {
    entities:[],
    entity:{},

  }, action) => {
  switch (action.type) {
    case "NEW_ENTITIES":
        return{
          ...state,
          entities:[...state.entities,action.payload]
        }
        case "GET_ENTITY":
        return{
          ...state,
          entity:action.payload
        }
        case "GET_ALL_ENTITIES":
          return{
            ...state,
            entities:action.payload
          
          }
         

    

    default: {
      return state
    }
  }
}
