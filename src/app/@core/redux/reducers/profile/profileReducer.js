//initialisation utilisateur
const initialState={
  

    profile:{},
    profiles:[],
    id:[]
  };
  
  
  
  export const profileReducer = (state = initialState, action) => {
    switch (action.type) {
      case "NEW_PROFILE": {
        return { ...state, profiles: action.payload }
      }
        case "GET_PROFILES":
          return{
            ...state,
            profiles:action.payload
  
          }
          case "GET_PROFILE":
          return{
            ...state,
            profile:action.payload
          }
        
      default: {
        return state
      }
    }
  }
  
  export default profileReducer