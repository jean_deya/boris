

//CREATE MESSAGE

export const createMessage=msg=>{
    return{
        type:"CREATE_MESSAGE",
        payload:msg
    }
}

//GET ERROR LOGIN
export const returnErrors=(msg,status)=>{
    return{
        type: "GET_ERRORS_LOGIN",
        payload:{msg,status}
    }
}