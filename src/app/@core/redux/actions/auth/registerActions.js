//import * as firebase from "firebase/app"
//import { history } from "../../../history"
//import "firebase/auth"
//import "firebase/database"
import {history} from "../../../../../history"
import{ returnErrors} from "./messageAction"
import axios from "axios"
import {createMessage} from "./messageAction"
//import { config } from "../../../authServices/firebase/firebaseConfig"
/*
// Init firebase if not already initialized
if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

let firebaseAuth = firebase.auth()

export const signupWithFirebase = (email, password, name) => {
  return dispatch => {
    let userEmail = null,
      loggedIn = false
    // userName = null

    firebaseAuth
      .createUserWithEmailAndPassword(email, password)
      .then(result => {
        firebaseAuth.onAuthStateChanged(user => {
          result.user.updateProfile({
            displayName: name
          })
          if (user) {
            userEmail = user.email
            // let userName = user.displayName
            loggedIn = true
            dispatch({
              type: "SIGNUP_WITH_EMAIL",
              payload: {
                email: userEmail,
                name,
                isSignedIn: loggedIn
              }
            })
            dispatch({
              type: "LOGIN_WITH_EMAIL",
              payload: {
                email: userEmail,
                name
              }
            })
          }
        })
        history.push("/")
      })
      .catch(error => {
        console.log(error.message)
      })
  }
}

*/
/*
export const signupWithJWT = (email, password, name) => {
  return dispatch => {
    axios
      .post("/api/authenticate/register/user", {
        email: email,
        password: password,
        name: name
      })
      .then(response => {
        var loggedInUser

        if(response.data){

          loggedInUser = response.data.user

          localStorage.setItem("token", response.data.token)

          dispatch({
            type: "LOGIN_WITH_JWT",
            payload: { loggedInUser, loggedInWith: "jwt" }
          })

          history.push("/")
        }

      })
      .catch(err => console.log(err))

  }
}

*/



//OBTENIR LISTE UTILISATEURS

export const listeUsers=()=>dispatch=>{
    
      axios.get("http://127.0.0.1:8080/v1/users/")
      // .then(reponse=>console.log("nous elevons",reponse.data.results))
      .then(res=>{

        dispatch({
          type:"GET_USERS",
          payload:res.data.results //pour envoyer la donn2e au reducer ou data:data
          }) 


             dispatch({
              type:"GET_LASTID",
              payload:res.data.count //pour envoyer la donn2e au reducer ou data:data
              }) 
            }
            )}
    
  
   //envoyer les donnes au reducer/sto
 //ENTITY

  //INSCRIPTION SUR LA PLATEFORME creation Entity
export const addEntity = ent => (dispatch, getState)=>{
  console.log("ajout entite")
 
 //let params = getState().dataList.params
   axios.post("http://127.0.0.1:8080t/v1/entities/",ent)
     
     .then(res => {
       dispatch({ 
         type: "NEW_ENTITIES", 
         payload:res.data 
       });
      // dispatch(getData(params))
     })
     .catch(err=>{
      returnErrors={
         msg:err.response.data,
         status:err.response.status
       };
       
   });
 };

//*GETONE ENTITE
export const getEntity = id => (dispatch, getState)=>{
  console.log("ajout entite")

 //let params = getState().dataList.params

   axios.get(`http://127.0.0.1:8080/v1/entities/${id}`)
     
     .then(res => {
       console.log("action",res.data)
       dispatch({ 
         type: "GET_ENTITY", 
         payload:res.data 
       });
      // dispatch(getData(params))
     })
     .catch(err=>{
        const returnErrors={
         msg:err.res.data,
         status:err.res.status
       };
       
   });
 };


//GETALLENTITY
export const getAllEntities=()=>dispatch=>{
    
  axios.get("http://127.0.0.1:8080/v1/entities/")
  // .then(reponse=>console.log("liste entities regiv",reponse.data.results))
  .then(res=>{

    dispatch({
      type:"GET_ALL_ENTITIES",
      payload:res.data.results //pour envoyer la donn2e au reducer ou data:data
      }) 


         dispatch({
          type:"GET_LASTID",
          payload:res.data.count //pour envoyer la donn2e au reducer ou data:data
          }) 
        }
        )}


                      //PROFILE
 //INSCRIPTION SUR LA PLATEFORME ajout Profil
export const addProfile = pro => (dispatch, getState)=>{
  console.log("ajout profile")
  console.log(pro)
 //let params = getState().dataList.params
   axios.post("http://127.0.0.1:8080/v1/profiles/",pro)
     
     .then(res => {
       dispatch({ 
         type: "NEW_PROFILES", 
         payload:res.data 
       });
      // dispatch(getData(params))
     })
     .catch(err=>{
       const errors={
         msg:err.response.data,
         status:err.response.status
       };
       
   });
 };
 

//INSCRIPTION SUR LA PLATEFORME ajout user
export const addUsers = user => (dispatch, getState)=>{
   console.log("inscription")
   console.log(user)
  //let params = getState().dataList.params
    axios.post("http://127.0.0.1:8080/v1/signup/",user)
      
      .then(res => {
        dispatch(
          createMessage({userAdded:"inscription éffectuée"
        }));
        dispatch({ 
          type: "INSCRIPTION", 
          payload:res.data 
        });
       // dispatch(getData(params))
      })
      .catch(err=>{
        const errors={
          msg:err.response.data,
          status:err.response.status
        };
        dispatch({
          type:"GET_ERRORS_REGISTER",
          payload:errors
        });
 
    });
  };
  

