import { history } from "../../../../../history"
import axios from "axios"
import { User } from "react-feather";
import{ returnErrors,createMessage} from "./messageAction"
import { config } from "react-transition-group";

export const loginWithJWT = user => {
  return dispatch => {
    axios
      .post("/api/authenticate/login/user", {
        email: user.email,
        password: user.password
      })
      .then(response => {
        let loggedInUser;
  
        if (response.data) {
          loggedInUser = response.data.user;
          localStorage.setItem('access_token', response.data.accessToken);
          dispatch({
            type: "LOGIN_WITH_JWT",
            isAuthenticated: true,
            payload: { loggedInUser, loggedInWith: "jt" },
          });
          history.push("/dashboard/apps")
        }
      })
      .catch(err => console.log(err))
  }
};

export const logoutWithJWT = () => {
  return dispatch => {
    dispatch({ type: "LOGOUT_WITH_JWT", payload: {} });
    history.push("/pages/login")
  }
};

export const logoutWithToken = () => {
  axios.post("http://rotten-cow-13.loca.lt/v1/auth/token/logout")
  return dispatch => {
    dispatch({ type: "LOGOUT_WITH_TOKEN", payload: {} });
    history.push("/")
  }
};

//check token end Load user

export const loadUser=()=>(dispatch,getState)=>{
  //user Loadng
  dispatch({type:"USER_LOADING"});

  // get token from state
  const auth_token= getState().auth.login.auth_token;
  console.log(auth_token)

 //Headers
 const config={
  headers:{
    "Content-Type":"application/json"
  }
};
      // apres avoir recu le token methode get
    //if token ,add to headers config
  
    if (auth_token){
      
      config.headers["Authorization"]=`Token ${auth_token}`;
    }
    
axios
    .get("http://127.0.0.1:8080/v1/auth/users/me",config)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    .then(res=>{
      dispatch({
        type:"USER_LOADED",
        payload:res.data

      });
    })
   
 .catch(err=>{
      dispatch(returnErrors(err.response.data,
        err.response.status));
        dispatch({
          type:"AUTH_ERROR"
        })
    })

}


/*TROIS ACTIONS POUR LE LOGIN AVEC TOKEN VIA DJANGO
//LOGIN_SUCCESS & LOGIN_FAIL  & LOGIN_ERROR*/


export const loginWithToken=(username,password)=>dispatch=>{

 //Headers
 const config={
 headers:{
    "Content-Type":"application/json"
  }
};                                                                                                                                                                                                                                   
 
  const body=JSON.stringify({username,password})
  
    axios.post("http://127.0.0.1:8080/v1/auth/token/login",body,config)
    .then(res=> { 
      dispatch(
      createMessage({userAuth:"vous êtes connecté !"
      }))
      dispatch({
        type:"LOGIN_SUCCESS",
       payload:res.data,
      });
      
    })
  
    .catch(err=>{
      dispatch(returnErrors(err.response.data,
        err.response.status));
        dispatch({
          type:"LOGIN_FAIL"
        })
    })

}

//LOGOUT USER


export const logout=()=>(dispatch,getState)=>{

  // get token from state
  const auth_token= getState().auth.login.auth_token;
  console.log(auth_token)

 //Headers
 const config={
  headers:{
    "Content-Type":"application/json"
  }
};
      // apres avoir recu le token methode get
    //if token ,add to headers config
  
    if (auth_token){
      
      config.headers["Authorization"]=`Token ${auth_token}`;
    }
    
axios.post("http://127.0.0.1:8080/v1/auth/token/logout/",null,config)
    .then(res=>{
      dispatch({
        type:"LOGOUT_SUCCESS"
      });
      history.push("/dashboard/auth")
    })
    
 .catch(err=>{
      dispatch(returnErrors(err.response.data,
        err.response.status));   
    });
};

