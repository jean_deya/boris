import axios from "axios"
import {createMessage} from "../auth/messageAction"

export const getData = params => {
  return async dispatch => {
    await axios.get("/api/datalist/data", params).then(response => {
      dispatch({
        type: "GET_DATA",
        data: response.data.data,
        totalPages: response.data.totalPages,
        params
      })
    })
  }
}

export const getInitialData = () => {
  return async dispatch => {
    await axios.get("/api/datalist/initial-data").then(response => {
      dispatch({ type: "GET_ALL_DATA", data: response.data })
    })
  }
}

export const filterData = value => {
  return dispatch => dispatch({ type: "FILTER_DATA", value })
}

export const deleteData = obj => {
  return dispatch => {
    axios
      .post("/api/datalist/delete-data", {
        obj
      })
      .then(response => {
        dispatch({ type: "DELETE_DATA", obj })
      })
  }
}

export const updateData = obj => {
  return (dispatch, getState) => {
    axios
      .post("/api/datalist/update-data", {
        obj
      })
      .then(response => {
        dispatch({ type: "UPDATE_DATA", obj })
      })
  }
}

export const addData = obj => {
  return (dispatch, getState) => {
    let params = getState().dataList.params
    axios
      .post("/api/datalist/add-data", {
        obj
      })
      .then(response => {
        dispatch({ type: "ADD_DATA", obj })
        dispatch(getData(params))
      })
  }
}

//Actions creation d'applications

export const createApp = obj => {
  return async dispatch => {
    await axios.post("http://127.0.0.1:8080/v1/applications/", obj)
    .then(response => {
        dispatch(
          createMessage({appAdded:"application ajoutée"
        }));
      dispatch({
        type: "CREATE_APP",
        payload: response.data,
       // totalPages: response.data.totalPages,
        //params
      })
    })
    .catch(err=>{
      const errors={
        msg:err.response.data,
        status:err.response.status
      };
      dispatch({
        type:"GET_ERRORS_CREATE_APP",
        payload:errors
      })
    })
  }
}
