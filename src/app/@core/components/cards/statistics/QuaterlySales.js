import React from "react"
import StatisticsCard from "../../../../@vuexy/statisticsCard/StatisticsCard"
import { Package } from "react-feather"
import { quaterlySales, quaterlySalesSeries } from "./StatisticsData"

class QuaterlySales extends React.Component {
  render() {
    return (
      <StatisticsCard
        icon={<Package className="danger" size={22} />}
        iconBg="danger"
        stat="0"
        statTitle="Nombre de documents terminés"
        options={quaterlySales}
        series={quaterlySalesSeries}
        type="area"
      />
    )
  }
}
export default QuaterlySales
