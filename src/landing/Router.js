import React, {Suspense} from "react";
import {Router, Switch, Route} from "react-router-dom";
import {history} from "../history";
import routes from "./routes";
import Layout from './@core/layouts';

function withLayout(WrappedComponent) {
  // ...and returns another component...
  return class extends React.Component {
    render() {
      return <Layout><WrappedComponent/></Layout>
    }
  };
}

class AppRouter extends React.Component {
  render() {
    return (
      // Set the directory path if you are deploying in sub-folder
      <Router history={history}>
        <Suspense fallback={<div />}>
          <Switch>
            {routes.map((route, idx) => {
              return <Route
                exact
                path={route.path}
                component={withLayout(route.component)}
                key={idx}/>
            })}
          </Switch>
        </Suspense>
      </Router>
    )
  }
}

export default AppRouter
