import React from 'react';

const Home = React.lazy(() => import('./views/Home/Home'));

const routes = [
    {path: '/', component: Home},
];

export default routes;
