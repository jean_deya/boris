import React, {Component} from 'react';
import Section from "../../@core/components/section";
import {Col, Row, } from "reactstrap";
//import startupSVG from "../../../assets/img/illustrator/Startup_SVG.png";
import img2 from "../../../assets/img/autres/sli.jpg";
import Features from "../../../landing/@core/layouts/components/features"
import Contacts from "../../../landing/@core/layouts/components/contacts"
import Slides from "../../@core/layouts/components/slides"
import Partner from "../../../landing/@core/layouts/components/carousel/partner"
import Pricing from "../../../landing/@core/layouts/components/Pricing"
import Temoignage from "../../../landing/@core/layouts/components/temoignage"
import JsonData from '../../@core/configs/data.json';
import Footer  from '../../../landing/@core/layouts/components/footer' 
import Services from '../../../landing/@core/layouts/components/services' 

export class Home extends Component {

    state = {
        landingPageData: {},
      }
      getlandingPageData() {
        this.setState({landingPageData : JsonData})
      }
    
      componentDidMount() {
        this.getlandingPageData();
      }

render(){
    return( <React.Fragment>
        <Slides/>
        <Services data={this.state.landingPageData.Services}/>
    
         
   <Features/>
   <Temoignage  data={this.state.landingPageData.Testimonials}/>
   <Pricing/>
   <Contacts  data={this.state.landingPageData.Contact}/>
   <Partner />
   <Footer/>
     
    </React.Fragment>
    )
}
}



export default Home