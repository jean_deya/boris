import React from 'react';
import {Container} from 'reactstrap';

function Section({children}) {
    return (
        <React.Fragment>
            <section className="bg-half-170 d-table w-100" id="home">
                <Container>
                    {children}
                </Container>
            </section>
        </React.Fragment>
    );
}

export default Section;
