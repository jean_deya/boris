import React, { Component } from 'react'
import {Card, Button, CardImg, CardTitle, CardText, CardGroup,
    CardSubtitle, CardBody} from "reactstrap";
import Particulier from "../../../../assets/img/user/01.jpg";
import img2 from "../../../../assets/img/autres/sli.jpg";
import img3 from "../../../../assets/img/autres/youtuber-2838945_640.jpg";
    

export class features extends Component {
    render() {
        return (
          
    <CardGroup className="features-section"  id="features">
      <Card>
        <CardImg top width="100%" src={Particulier}alt="Card image cap" />
        <CardBody>
          <CardTitle> SCANNER POUR LES ENTREPRISES</CardTitle>
          <CardSubtitle></CardSubtitle>
          <CardText>  Adapté egalement aux entreprises.la reconnaissance de facture vous permettra d enregistrer vos recus sans perdre le temps a tout faire a la main 
            ou certains logiciel moins adaptes.</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src={img2} alt="Card image cap" />
        <CardBody>
          <CardTitle> SCANNER POUR LES AUTO ENTREPRENEURS</CardTitle>
          <CardSubtitle></CardSubtitle>
          <CardText>Etes vous a votre compte? Vous n'avez pas de grandes notions en comptabilite..N'hesitez plus,avec notre technologie.
            vous n'auriez plus de soucis a faire vos comptes.Vous pourriez ainsi vous concentrer sur le plus important de votre travail.
            </CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
      <Card>
        <CardImg top width="100%" src={img3} alt="Card image cap" />
        <CardBody>
          <CardTitle>POUR LES COMPTABLES</CardTitle>
          <CardSubtitle></CardSubtitle>
          
          <CardText>
            
          </CardText>
     
          <Button>Button</Button>
        </CardBody>
      </Card>
    </CardGroup>
    
        )
    }
}

export default features
