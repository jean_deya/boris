import React, { Component } from 'react'
import data from "../../../configs/datas"
import {Row,Col,Button } from "reactstrap"
import Card from "./Card"
import "./style.scss"




export default class partner extends Component {
   constructor(props){
       super(props)
       this.state={
            partners:data.partners,
            partner:data.partners[0]
          
           
       } 
   }

  nextPartner =()=>{
      const nextIndex=this.state.partner.index +1;
      this.setState({
          partner:data.partners[nextIndex]
      })

  }
  prevPartner=()=>{
    const nextIndex=this.state.partner.index-1;
    this.setState({
        partner:data.partners[nextIndex]
    })

}

  
render() {
    const  {partner,partners}=this.state
        
    return (
    <div id="partner">
        <Row>
            <Col xs="6" sm="4">
                <Button
                    onClick={()=>this.prevPartner()}
                    disabled={partner.index===0}
                    >precedent
                </Button>    
            </Col>
            <Col style={{alignItems:"center",marginLeft: "auto",
  marginRight:"auto",}} xs="6" sm="4">
                <h1 style={{textAlign:"center"}}>Nos partenaires</h1> 
            </Col>
            <Col sm="4">
                <Button
                    
                    style={{marginLeft:"300px"}}
                    onClick={()=>this.nextPartner()}
                    disabled={partner.index===
                    data.partners.length -1}
                    >
                    suivant
                </Button>
        
            </Col>
      </Row>
         <div className="cole">
         <div className={`cards-slider active-slide-${partner.index}`}>
             <div className="cards-slider-wrapper" 
             style={{
                 'transform':`translateX(-${partner.index*(90/partners.length)}%)`
             }}
            >
             {
                partners.map(partner=>
                    <Card key={partner._id}
                    partner={partner}
                    />
                    ) 
             }
                  <Card partner={partner}/>
            
            </div>
        </div>
        </div>
       
    </div>
        )
    }
}
