import React from 'react';
import {CardImg} from "reactstrap"


const Card =({partner})=> {
    const{index,image}=partner
    return (
        <div id={`card-${index}`} className="card">
            <CardImg  style={{ minHeight:"225px"}}  src={image} alt="partenaire"  />          
        </div>
    )
}


export default Card ;