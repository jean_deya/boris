import React, { Component } from 'react'
import letterVideo from "../components/carousel/letter.mp4"
import { Button ,Col,Row,Container} from 'reactstrap';
export default class slides extends Component {
  

    render() {
        return (
            <div className="v-header containers" >
                <div className="fullscreen-video-wrap">
                <video autoPlay loop muted>
                    <source src={letterVideo} type="video/mp4"/>               
                    </video>
                </div>
                <div className="header-overlay">
                    <div className="header-content">
                    <h1 style={{color:"#156493",
                            alignItems:"center",
                            textAlign:"center",
                            marginTop:"180px",
                            backgroundColor:"",
                            minHeight:"20px"
                
                
                 }} >Notre plateforme d'OCR conçue pour vous !</h1>
                    
                   
                    <h3 style={{textAlign:"center",color:" white"}} >Avec Scanner, fini les reçus égarés et les erreurs de factures !
                              La première application intelligente pour scanner et analyser les documents.
                        </h3>
                        </div>
  <Container style={{position:"relative",marginLeft:"200px"}}>

      <Row>
        <Col sm={{ size: 'auto', offset: 1 }}></Col>
        <Col sm={{ size: 'auto', offset: 1 }}>
        <Button style={{marginLeft:"-10px",color:"green"}} color="info" size="lg" active><a href="/" className="btn">Nous contacter</a></Button>
       </Col>
       <Col>
        <Button outline color="info"><a href="/" className="btn">Essayer</a></Button>
        </Col>
      </Row>
</Container>

        
    </div>
    </div>
        )
    }
}
