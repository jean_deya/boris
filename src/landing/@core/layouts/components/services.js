import React, { Fragment } from "react";
import "./services.scss";
import Fade from "react-reveal/Fade"

import {Col, Row,Container } from "reactstrap";
import imgGirl from "./images/girl.webp";

 function Services(props){
  
  const {data}=props
  
  
    
  return(
    <div id="services" className="text-center" >
        <div className="container" >
          <div className="section-title">
            <h2>Nos Services</h2>
          
          </div>
          <div className="row">
           {/*data
              ? data.map((d, i) => (
                  <div  key={`${d.name}-${i}`} className="col-md-4">
                    {" "}
                    <i className={d.icon}></i>
                    <div className="service-desc">
                      <h3>{d.name}</h3>
                      <p>{d.text}</p>
                    </div>
                  </div>
                ))
              : "loading"*/}
      <Row>
        {
          data
          ? data.map((d, i) =>(
            <Fragment>
              <Col xs="6" key={`${d.name}-${i}`} >
                <Fade top>
              {" "}
            <div className="hero-content-inner" >
                <h1>
                        <div className="hero-content-line">
                          <div className="hero-content-line-inner">{d.name}
                        
                          
                        </div>
                        </div>
                      </h1>
                      <p>
                        {d.text}
                      </p>
            </div>

            </Fade>

                </Col>
                
                <Col xs="6">
                  <Fade bottom>
            
                    <div className="hero-images-inner">
                      
                        <img width="300px"  src={d.image} alt="girl" />
                    
                    </div>
                    <br/>
                    </Fade>
                </Col>
                  <br/>
                 
                </Fragment>

        ))
              : "loading"}
      </Row>
              
            
          
          </div>
        </div>
      </div>
    );
  }


  export default Services;

  
  /*return (
    <div className="hero" ref={el => services = el}>
      <div className="contain">
        <div className="hero-inner">
          <div className="hero-content">
            <div className="hero-content-inner" ref={el => content = el}>
              <h1>
                <div className="hero-content-line">
                  <div className="hero-content-line-inner">Relieving the burden</div>
                </div>
                <div className="hero-content-line">
                  <div className="hero-content-line-inner">of disease caused</div>  
                </div>
                <div className="hero-content-line">
                  <div className="hero-content-line-inner">by behaviors.</div>
                </div>
              </h1>
              <p>
                Better treats serious cardiometabolic diseases to transform
                lives and reduce healthcare utilization through the use of
                digital therapeutics.
              </p>
              <div className="btn-row">
                <button className="explore-button">Explore
                  <div className="arrow-icon">
                    <img src={arrow} alt="row"/>
                  </div>
                </button>
              </div>
            </div>
          </div>
          <div className="hero-images">
            <div ref={el => images = el}  className="hero-images-inner">
              <div className="hero-image girl">
                <img src={imgGirl} alt="girl" />
              </div>
              <div className="hero-image boy">
                <img src={imgBoy} alt="boy" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}*/

