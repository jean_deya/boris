import React, { Component } from "react";
import {User, Mail,Lock, Coffee} from "react-feather"
import {
    Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form, 
  Button,
  Label
 }from "reactstrap"
 import Partner from "./carousel/partner"

export class Contact extends Component {
  render() {
    
    return (
      <div>
        <div className="contact-section" id="contact">
          <div className="container">
            <div className="col-md-8">
              <div className="row">
                <div className="section-title">
                  <h2>Nous contacter ?</h2>
                  <p>
                    Veuillez remplir ce formulaire ,nous nous ferions un plaisir de vous repondre dans les
                    plus bref delais !
                  </p>
                </div>
                <form name="sentMessage" id="contactForm" noValidate>
                  <div className="row">
                    <div className="col-md-6">
                    <FormGroup className="has-icon-left position-relative">
                        <input
                          type="text"
                          id="nom"
                          className="form-control"
                          placeholder="Nom"
                          required="required"
                        />
                         <div className="form-control-position">
                        <User style={{alignItems:"left",paddingLeft:" 10px"}} color="red" size={15} />
                        </div>
                        <p className="help-block text-danger"></p>
                      </FormGroup>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <Input
                          type="email"
                          id="email"
                          className="form-control"
                          placeholder="Email"
                          required="required"
                        />
                        <p className="help-block text-danger"></p>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <textarea
                      name="message"
                      id="message"
                      className="form-control"
                      rows="4"
                      placeholder="Message"
                      required
                    ></textarea>
                    <p className="help-block text-danger"></p>
                  </div>
                  <div id="success"></div>
                  <button id="buyButton" className="btn btn-primary">
                    Envoyer
                  </button>
                </form>
              </div>
            </div>
            <div className="col-md-3 col-md-offset-1 contact-info">
              <div className="contact-item">
                <h3>Nos Contacts</h3>
                <p>
                  <span>
                    <i className="fa fa-map-marker"></i> Addresse
                  </span>
                  {this.props.data ? this.props.data.address : "loading"}
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="fa fa-phone"></i> Telephone
                  </span>{" "}
                  {this.props.data ? this.props.data.phone : "loading"}
                </p>
              </div>
              <div className="contact-item">
                <p>
                  <span>
                    <i className="fa fa-envelope-o"></i> Email
                  </span>{" "}
                  {this.props.data ? this.props.data.email : "loading"}
                </p>
              </div>
            </div>
            <div className="col-md-12">
              <div className="row">
                <div className="social">
                  <ul>
                    <li>
                      <a
                        href={this.props.data ? this.props.data.facebook : "/"}
                      >
                        <i className="fa fa-facebook"></i>
                      </a>
                    </li>
                    <li>
                      <a href={this.props.data ? this.props.data.twitter : "/"}>
                        <i className="fa fa-twitter"></i>
                      </a>
                    </li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      
    );
  }
}

export default Contact;