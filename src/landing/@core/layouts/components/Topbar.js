import React, {Component} from 'react';
//import {history} from "../../../../history"
import {connect} from "react-redux"
import PropTypes from  "prop-types"

import {withRouter} from "react-router";
import {Container} from "reactstrap";
//scrool
import { Link, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'
import logo from "../../../../assets/img/footer/scanner.png"
class Topbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
      isOpen: false,
   /*   navLinks: [
        {id: 1, title: "Accueil", link: "/"},
        {id: 1, title: "features", link: "#features"},
        {id: 1, title: "Tarification", link: "/"},
        {id: 1, title: "Contact", link: "/"},
      ]*/
    };
    this.toggleLine = this.toggleLine.bind(this);
    this.openBlock.bind(this);
    this.openNestedBlock.bind(this);

  }

  toggleLine() {
    this.setState(prevState => ({isOpen: !prevState.isOpen}));
  }

  componentDidMount() {
  /*  let matchingMenuItem = null;
    const ul = document.getElementById("top-menu");
    const items = ul.getElementsByTagName("a");
    for (let i = 0; i < items.length; ++i) {
      if (this.props.location.pathname === items[i].pathname) {
        matchingMenuItem = items[i];
        break;
      }
    }
    if (matchingMenuItem) {
      this.activateParentDropdown(matchingMenuItem);
    }*/
  }

  activateParentDropdown = (item) => {
    const parent = item.parentElement;
    if (parent) {
      parent.classList.add('active'); // li
      const parent1 = parent.parentElement;
      parent1.classList.add('active'); // li
      if (parent1) {
        const parent2 = parent1.parentElement;
        parent2.classList.add('active'); // li
        if (parent2) {
          const parent3 = parent2.parentElement;
          parent3.classList.add('active'); // li
          if (parent3) {
            const parent4 = parent3.parentElement;
            parent4.classList.add('active'); // li
          }
        }
      }
    }
  };

  openBlock = (level2_id) => {
    //match level 2 id with current clicked id and if id is correct then update status and open level 2 submenu
    this.setState({
      navLinks: this.state.navLinks.map(navLink => (navLink.id === level2_id ? {
        ...navLink,
        isOpenSubMenu: !navLink.isOpenSubMenu
      } : navLink))
    });
  };

  openNestedBlock = (level2_id, level3_id) => {
    const tmpLinks = this.state.navLinks;
    tmpLinks.map((tmpLink) =>
      //Match level 2 id
      tmpLink.id === level2_id ?
        tmpLink.child.map((tmpchild) =>
          //if level1 id is matched then match level 3 id
          tmpchild.id === level3_id ?
            //if id is matched then update status(level 3 sub menu will be open)
            tmpchild.isOpenNestedSubMenu = !tmpchild.isOpenNestedSubMenu
            :
            tmpchild.isOpenNestedSubMenu = false
        )
        :
        false
    );
    this.setState({navLinks: tmpLinks});
  };

  render()   {
    console.log("isauht",this.props.isAuthenticated)
    return (
      <React.Fragment>
        <header id="topnav" className="defaultscroll sticky active ">
          <Container>
            <div>
              <a className="logo" href="/"> <img src={logo} alt="logo scanner"/></a>
            </div>
            <React.Fragment>
            {this.props.isAuthenticated ===true? (
           <div className="getting-started-button" style={{marginLeft: "10px"}}>
           <a href='/dashboard/ChoixProfile' id="buyButton" className="btn btn-primary">choix Profile</a>
         </div>
      
            
            
              
            ) : <div>
            <div className="getting-started-button" style={{marginLeft: "10px"}}>
              <a href='/dashboard/' id="buyButton" className="btn btn-primary">Essayer</a>
            </div>
          </div>}
          </React.Fragment>
            <div className="menu-extras">
              <div className="menu-item">
                <Link to="#" onClick={this.toggleLine}
                      className={this.state.isOpen ? "navbar-toggle open" : "navbar-toggle"}>
                  <div className="lines">
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                </Link>
              </div>
            </div>
            <div className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"  style={{display: this.state.isOpen ? "block" : "none"}}>
          <div className="container">
            <ul className="navigation-menu" id="top-menu">
                <li>
                    <Link activeClass="active" className="has-submenu"
                          to="services"
                          spy={true}
                          smooth={true}
                          hashSpy={true}
                          offset={0}
                          duration={400}
                          delay={1000}
                          isDynamic={true}
                          onSetActive={this.handleSetActive}
                          onSetInactive={this.handleSetInactive}
                          ignoreCancelEvents={false}
                        >
                        Services
                      </Link>
                  </li>
                  <li>
                    <Link activeClass="active" className="has-submenu"
                    to="about"
                      spy={true}
                      smooth={true}
                      hashSpy={true}
                      offset={0}
                      duration={400}
                      delay={1000}
                      isDynamic={true}
                      onSetActive={this.handleSetActive}
                      onSetInactive={this.handleSetInactive}
                      ignoreCancelEvents={false}
                      >
                      A propos
                      </Link>
                  </li>
                  <li>
                    <Link activeClass="active" className="has-submenu"
                          to="temoignages"
                          spy={true}
                          smooth={true}
                          hashSpy={true}
                          offset={1}
                          duration={400}
                          delay={1000}
                          isDynamic={true}
                          onSetActive={this.handleSetActive}
                          onSetInactive={this.handleSetInactive}
                          ignoreCancelEvents={false}
                          >
                          Temoignages
                      </Link>
                    </li>
                     <li>
                      <Link activeClass="active" className="has-submenu"
                          to="pricing"
                          spy={true}
                          smooth={true}
                          hashSpy={true}
                          offset={-10}
                          duration={400}
                          delay={1000}
                          isDynamic={true}
                          onSetActive={this.handleSetActive}
                          onSetInactive={this.handleSetInactive}
                          ignoreCancelEvents={false}
                         >
                        Tarification
                        </Link>
                  </li>
                  <li>
                        <Link activeClass="active" className="has-submenu"
                        to="contact"
                        spy={true}
                        smooth={true}
                        hashSpy={true}
                        offset={0}
                        duration={400}
                        delay={1000}
                        isDynamic={true}
                        onSetActive={this.handleSetActive}
                        onSetInactive={this.handleSetInactive}
                        ignoreCancelEvents={false}
                      >
                      Contacts
                  </Link>
              </li>
            </ul>
          </div>
         
      
              <div className="buy-menu-btn d-none">
                <Link to="#" target="_blank" className="btn btn-primary">Buy Now</Link>
              </div>
            </div>
          </Container>
        </header>
      </React.Fragment>
    );
  }
}

Topbar.propTypes={

  username:PropTypes.string,
  isAuthenticated:PropTypes.bool
}


const mapStateToProps= (state)=>{
  return{
    isAuthenticated:state.auth.login.isAuthenticated
   
  }
}


export default connect(mapStateToProps)(withRouter(Topbar))


