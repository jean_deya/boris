import React, {Suspense} from 'react';
import {withRouter} from 'react-router-dom';

// Layout Components
const Topbar = React.lazy(() => import('./components/Topbar'));

function Layout({children}) {
    return (
        <React.Fragment>
            <Suspense fallback={<div/>}>
                <Topbar/>
                {children}
            </Suspense>
        </React.Fragment>
    );
}

export default withRouter(Layout);
