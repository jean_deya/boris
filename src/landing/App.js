
import React from 'react';
import Router from "./Router"

// Import Css
import './Apps.scss';
import '../assets/css/materialdesignicons.min.css';
import '../assets/css/colors/default.css';

const App = () => {
  return <Router />
};

export default App
